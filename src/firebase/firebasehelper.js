import * as firebase from 'firebase';


const firebaseAuth = firebase.auth;
const provider = new firebase.auth.GoogleAuthProvider();

class FirebaseHelper {

    constructor() {
        this.login = this.login.bind(this);
        this.logout = this.logout.bind(this);
        this.isAuthenticated = this.isAuthenticated.bind(this);
    }

    login() {
        return firebase.auth().signInWithPopup(provider)
    }

    logout() {
        return firebaseAuth().signOut();
    }

    isAuthenticated() {
        firebaseAuth().onAuthStateChanged(user => {
            return user ? true : false;
        });
    }
}

export default new FirebaseHelper();
