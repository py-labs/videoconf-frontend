import * as firebase from 'firebase';

const config = {
  apiKey: 'AIzaSyAD5NaOvsR0AVppI6-reILnLRzzY0sHtoI',
  authDomain: 'medchat-30d41.firebaseapp.com',
  databaseURL: 'https://medchat-30d41.firebaseio.com/',
  projectId: 'medchat-30d41',
  storageBucket: 'medchat-30d41.appspot.com',
  messagingSenderId: '760485850540'
};

if (!firebase.apps.length) {
    firebase.initializeApp(config);
}
const firebaseAuth = firebase.auth;

const database = firebase.database();
const provider = new firebase.auth.GoogleAuthProvider();
// firebase.auth().signInWithPopup(provider);


export { firebaseAuth, firebase, database };
