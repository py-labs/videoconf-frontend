import React, {Component} from 'react';
import ConferenceWindow from "../../components/ConferenceWindow/ConferenceWindow";

class Conference extends Component {
    render() {
        return (

            <div className="row">
                <div className="wrapper wrapper-content {/*animated fadeInRight*/}">
                    <ConferenceWindow/>
                </div>
            </div>

        )
    }
}


export default Conference;