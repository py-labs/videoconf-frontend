import React, {Component} from 'react'
import AppointmentWindow from "../../components/AppointmentWindow/AppointmentWindow"

class Appoint extends Component {
    render() {
        return (
            <div className="row">
                <div className="col-lg-12">
                    <div className="wrapper wrapper-content {/*animated fadeInUp*/}">
                        <AppointmentWindow />
                    </div>
                </div>
            </div>

        )
    }
}

export default Appoint;