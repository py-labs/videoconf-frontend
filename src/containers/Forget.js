import React from 'react'
import {connect} from 'react-redux'
import {Redirect} from 'react-router'

import ForgetForm from '../components/ForgetForm'
import {isAuthenticated} from '../store/reducers'

const Forget = (props) => {
    if (props.isAuthenticated) {
        return (
            <Redirect to='/'/>
        )
    } else {
        return (
            <div className="login-page">
                <ForgetForm {...props}/>
            </div>

        )
    }
}

const mapStateToProps = (state) => ({
    isAuthenticated: isAuthenticated(state)
})

export default connect(mapStateToProps)(Forget);