import React, {Component} from 'react';
import {connect} from 'react-redux';
import {calendarSelectedFilters, recordList} from "../../store/reducers";
import './fullcalendar.css'
import './PatientCalendar.css'
import {CLEAR_APPOINTMENTS, requestRecordsAppointments, SET_AVAILABLE_TIME} from "../../store/appointments/actions";
import {Button} from "reactstrap";


class PatientCalendar extends Component {
    state = {
        date: this.props.value,
        month: this.props.value.getMonth(),
        year: this.props.value.getFullYear(),
        records: this.props.records,
        doctors: {},
        freeDates: [],
        availableDates: [],
        currIndexArr: 0,
        countDates: 5
    };
    monthNames = ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь",
        "Ноябрь", "Декабрь"];



    componentDidUpdate(prevProps) {
        // Typical usage (don't forget to compare props):
        if (this.props.selectedFilters !== prevProps.selectedFilters) {
            const {selectedFilters} = this.props;

            this.props.dispatch(requestRecordsAppointments(
                selectedFilters.dateStart,
                selectedFilters.dateEnd,
                selectedFilters.doctor,
                selectedFilters.specialization,
                selectedFilters.place)
            );
        }

        if (this.props.records !== prevProps.records) {
            this.setState({
                records: this.props.records
            });
            const dates = this.getFreeDates();
            this.setState({
                freeDates: dates
            })
        }
    }

    componentWillUnmount() {
        this.props.dispatch({
            type: CLEAR_APPOINTMENTS
        })
    }

    checkDoctorExist = (doctors, doctor) => {
        return doctors.findIndex(i => i.pk === doctor.pk) > 0;
    }

    saveDoctors = (doctors) => {
        this.setState({
            doctors: doctors
        })
    };

    compareNumeric(a, b) {
        if (a > b) return 1;
        if (a < b) return -1;
    }

    getFreeDates() {
        let dates = {};
        let doctors = {};
        let doctorRows = [];
        let dates2 = [];

        const {records} = this.props;

        for (let i = 0; i < records.length; i++) {
            let stringDate = records[i].date_record.split('-');
            let dateTime = new Date(stringDate).getTime();

            if (dates2.indexOf(dateTime) < 0)
                dates2.push(dateTime);

            if (!dates.hasOwnProperty(dateTime))
                dates[dateTime] = [];
            const doctor = records[i].doctor;
            const doctorId = doctor.pk.toString();

            if (!dates[dateTime].hasOwnProperty(doctorId))
                dates[dateTime][doctorId] = [];

            dates[dateTime][doctorId].push({
                'times': records[i].available_time,
                'place': records[i].place
            });

            if (!doctors.hasOwnProperty(doctor.pk)) {
                doctors[doctor.pk] = doctor.user_full_name
            }

            if (!doctorRows.hasOwnProperty(doctorId))
                doctorRows[doctorId] = [];

            let indexObj = null;
            for (let i = 0; i < doctorRows[doctorId].length; i++) {
                let dateRecord = doctorRows[doctorId][i];
                let currentDate = parseInt(Object.keys(dateRecord)[0]);

                if (currentDate === dateTime) {
                    indexObj = i;
                    break;
                }
            }
            if (indexObj) {
                doctorRows[doctorId][indexObj][dateTime].push({
                    'times': records[i].available_time,
                    'place': records[i].place
                })
            }
            else {
                let dateRecord = {};
                dateRecord[dateTime] = [];
                dateRecord[dateTime].push({
                    'times': records[i].available_time,
                    'place': records[i].place
                });
                doctorRows[doctorId].push(dateRecord);
            }


        }
        dates2 = dates2.sort(this.compareNumeric);

        this.setState({
            availableDates: dates2
        });

        this.saveDoctors(doctors);
        return doctorRows;
    }

    renderHeaderTable() {

        const {availableDates, currIndexArr, countDates} = this.state;
        let rows = [];

        const selectedDates = availableDates.slice(currIndexArr, currIndexArr + countDates);


        const countDatesInRow = selectedDates.length;
        const widthItem = (100 / (countDates + 1)) + '%';

        if (countDatesInRow > 0){
            rows.push(
                <div style={{'width': widthItem}} className="header-month" key={"DoctorHeaderName-"+widthItem}>
                    Врач
                </div>
            );
            for (let i = 0; i < countDatesInRow; i++) {
                const currDate = new Date(parseInt(selectedDates[i], 10));
                rows.push(
                    <div style={{'width': widthItem}} className="header-month" key={currDate.getTime()}>
                        {currDate.getDate() + '.' + (currDate.getMonth() + 1) + '.' + currDate.getFullYear()}
                    </div>
                )
            }
        }

        return rows;
    }

    getWeeks() {
        let rows = [];
        if (this.state.records.length) {
            const {freeDates, availableDates, currIndexArr, countDates} = this.state;
            const selectedDates = availableDates.slice(currIndexArr, countDates + currIndexArr);

            const countDatesInRow = selectedDates.length;
            const widthItem = (100 / (countDates + 1)) + '%';
            for (const [pkDoctor, content] of Object.entries(freeDates)) {
                let doctor = {
                    'pk': pkDoctor,
                    'name': this.state.doctors[pkDoctor]
                };
                rows.push(
                    <DoctorRow
                        key={"doctor-row-" + pkDoctor}
                        requiredDays={selectedDates}
                        widthItem={widthItem}
                        calendar={this}
                        doctor={doctor}
                        items={content}
                    />
                )
            }

        }
        return rows;
    }

    getNextDates = () => {
        const {availableDates, currIndexArr, countDates} = this.state;
        if (currIndexArr + countDates < availableDates.length) {
            this.setState({
                currIndexArr: currIndexArr + countDates
            })
        }
    };

    getBackDates = () => {
        if (this.state.currIndexArr > 0) {
            this.setState({
                currIndexArr: this.state.currIndexArr - this.state.countDates
            })
        }
    };

    renderCalendar = () => {
        return (
            <div className="ibox">
                <div className="ibox-content">
                    {/*<div className="days-container">*/}
                    <div className="row table-header">
                        <div className="col-xs-offset-3 col-xs-6 text-center calendar-title">
                            Выберите время
                        </div>
                        <div className="col-xs-3 text-right">
                            <Button className="fc-prev-button fc-button fc-state-default fc-corner-left"
                                    onClick={this.getBackDates}>
                                <span className="fc-icon fc-icon-left-single-arrow"/>
                            </Button>
                            <Button className="fc-next-button fc-button fc-state-default fc-corner-right"
                                    onClick={this.getNextDates}>
                                <span className="fc-icon fc-icon-right-single-arrow"/>
                            </Button>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-xs-12">
                            {this.renderHeaderTable()}
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-xs-12">
                            {this.getWeeks()}
                        </div>
                    </div>

                </div>
            </div>
        )
    }

    render() {
        const {selectedFilters} = this.props;
        const {records} = this.state;

        // check no empty filters
        if (Object.keys(selectedFilters).length && records.length) {
            return this.renderCalendar();
        }
        else
            return (
                <div>Выберите фильтры или свободного времени не найдено.</div>
            )
    }
}

class DoctorRow extends Component {
    searchDay = (dateTime) => {
        const {items} = this.props;
        for (let i = 0; i < items.length; i++) {
            let timeDate = parseInt(Object.keys(items[i])[0]);
            if (timeDate === dateTime) {
                return items[i][timeDate];
            }
        }
        return null;
    };

    renderDays() {
        const {doctor, widthItem, requiredDays, calendar} = this.props;
        let days = [];
        for (let i = 0; i < requiredDays.length; i++) {
            const requiredDay = requiredDays[i];
            const timeDate = this.searchDay(requiredDay);
            let currDate = new Date(parseInt(requiredDay, 10));
            days.push(
                <Day
                    key={requiredDay + "doctor-" + doctor.pk + 'index-' + i}
                    doctor={doctor}
                    calendar={calendar}
                    date={currDate}
                    times={timeDate !== null ? timeDate : []}
                    widthItem={widthItem}
                />
            )
        }
        return days;
    }

    render() {
        const {doctor, widthItem} = this.props;
        return (
            <div className="doctor-row">
                <div className="doctor-name" style={{'width': widthItem}}>{doctor.name}</div>
                {this.renderDays()}
            </div>
        )
    }
}


class Day extends Component {
    state = {
        number: this.props.number,
        month: this.props.month,
        year: this.props.year,
        timesRecord: this.props.timesRecord,
        calendar: this.props.calendar
    }

    setSelectedAvailableTimes(time, doctor, place) {
        this.state.calendar.props.dispatch({
            type: SET_AVAILABLE_TIME,
            payload: {
                'date': this.props.date.getFullYear() + '-' + (this.props.date.getMonth() + 1) + '-' + this.props.date.getDate(),
                'time': time,
                'doctor': doctor,
                'place': place
            }
        })
    }

    getAvailableTimesAll() {
        const {times, doctor, date} = this.props;
        let rows = [];
        for (let i = 0; i < times.length; i++) {
            rows.push(this.getAvailableTimes(times[i]));
        }

        if (times.length === 0) {
            rows.push(
                <div
                    className="col-xs-12"
                    key={"emptyDate" + "-doctor-" + doctor.pk + "-date-" + date.getTime()}>
                    &nbsp;
                </div>
            )
        }
        return rows;
    }

    getAvailableTimes(timesAndPlace) {
        const times = [];
        const timesRecordList = timesAndPlace.times;
        const place = timesAndPlace.place;
        const {doctor, date} = this.props;

        times.push(
            <div className="col-xs-12 place-name"
                 key={"doctor-" + doctor.pk + "-date-" + date.getTime() + "place-" + place.id}>
                {place.name}
            </div>
        );

        timesRecordList && timesRecordList.map(time => {
            times.push(
                <div className="col-xs-12 record-time"
                     key={"doctor-" + doctor.pk + "-date-" + date.getTime() + '-time-' +
                     time.time + "-" + "-place-" + place.id}
                     onClick={this.setSelectedAvailableTimes.bind(this, time, doctor, place)}>
                    {time.time}
                </div>
            )

        });

        return times;
    }

    render() {
        const {widthItem} = this.props;
        return (
            <div className="calendar-date" style={{'width': widthItem}}>
                {this.getAvailableTimesAll()}
            </div>
        )
    }
}


const mapStateToProps = (state) => ({
    records: recordList(state),
    selectedFilters: calendarSelectedFilters(state)
})


export default connect(mapStateToProps, null)(PatientCalendar);
