import React, {Component} from 'react'
import EventsList from "../../components/EventsList/EventsList"

class Events extends Component {
    render() {
        return (
            <div className="row">
                <div className="col-lg-12">
                    <div className="wrapper wrapper-content {/*animated fadeInUp*/}">
                        <EventsList />
                    </div>
                </div>
            </div>

        )
    }
}

export default Events;