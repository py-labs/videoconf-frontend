import React, {Component} from 'react'
import MedcartList from "../../components/MedcartList/MedcartList"

class Medcart extends Component {
    render() {
        return (
            <div className="row">
                <div className="col-lg-12">
                    <div className="wrapper wrapper-content {/*animated fadeInUp*/}">
                        <MedcartList />
                    </div>
                </div>
            </div>

        )
    }
}

export default Medcart;