import React, {Component} from 'react'
import OptionsList from "../../components/OptionsList/OptionsList"
import ProfileHeader from "../../components/ProfileHeader/ProfileHeader"

class Profile extends Component {
    render() {
        return (
            <div className="row">
                <div className="col-lg-12">
                    <div className="wrapper wrapper-content {/*animated fadeInUp*/}">
                        <ProfileHeader />
                        <OptionsList />
                    </div>
                </div>
            </div>

        )
    }
}

export default Profile;