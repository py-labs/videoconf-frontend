import React, {Component} from 'react'
import {connect} from "react-redux";
import * as reducers from '../../store/reducers'
import VideoWindowIcons from "../../components/VideoWindowIcons/VideoWindowIcons";
import {USER_NOT_READY_FOR_VIDEO_CALL} from "../../store/user/actions";
import Fullscreen from "react-full-screen";
import './PatientVideoWindow.css'
import PatientAcceptCallModalWindow from '../../components/PatientAcceptCallModalWindow/PatientAcceptCallModalWindow'

const SERVER_EASYRTC_ADDRESS = process.env.REACT_APP_SERVER_EASYRTC_ADDRESS || "localhost:8080";


class PatientVideoWindow extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isFull: false,
            isOpenModalWindow: false
        }
    }

    loginSuccess(easyrtcid) {
        // console.log('login');
        // console.log(easyrtcid);
        // let selfEasyrtcid = easyrtcid;
        // this.setEasyId(easyrtcid);
        // document.getElementsByClassName("iam")[0].innerHTML = "I am " + window.easyrtc.cleanId(easyrtcid);
        // document.getElementsByClassName("iam")[0].innerHTML = "I am " + window.easyrtc.cleanId(this.state.username);
    }

    loginFailure(errorCode, message) {
        window.easyrtc.showError(errorCode, message);
    }

    requestFullscreen = function (ele) {
        if (ele.requestFullscreen) {
            ele.requestFullscreen();
        } else if (ele.webkitRequestFullscreen) {
            ele.webkitRequestFullscreen();
        } else if (ele.mozRequestFullScreen) {
            ele.mozRequestFullScreen();
        } else if (ele.msRequestFullscreen) {
            ele.msRequestFullscreen();
        } else {
            console.log('Fullscreen API is not supported.');
        }
    };

    closeVideoStream() {
        window.easyrtc.disconnect();
        window.easyrtc.clearMediaStream(document.getElementById('selfVideo'));
        window.easyrtc.closeLocalMediaStream();
    }

    componentWillUnmount() {
        this.closeVideoStream();

        this.props.dispatch({
            type: USER_NOT_READY_FOR_VIDEO_CALL
        })
    }

    acceptChecker = (otherGuy, acceptorCallback) => {
        this.setState({
            isOpenModalWindow: true,
            acceptorCallback: acceptorCallback
        });
    };

    componentDidUpdate(prevProps) {
        const {isReadyForVideoCall} = this.props;

        if (isReadyForVideoCall !== prevProps.isReadyForVideoCall) {
            if (isReadyForVideoCall) {
                window.easyrtc.hangupAll();
                window.easyrtc.setVideoDims(640, 480);
                window.easyrtc.setUsername(this.props.username);
                window.easyrtc.setSocketUrl(SERVER_EASYRTC_ADDRESS);
                window.easyrtc.easyApp("easyrtc.audioVideoSimple", "selfVideo", ["callerVideo"], this.loginSuccess.bind(this), this.loginFailure.bind(this));
                window.easyrtc.setAcceptChecker(this.acceptChecker);
            }
            else {
                this.closeVideoStream();
            }
        }
    }


    goFull = () => {
        const {isFull} = this.state;
        this.setState({ isFull: !isFull });
    };

    handleAccept = (isAccept) => {
        this.setState({isOpenModalWindow: isAccept});
    };

    render() {
        const {isReadyForVideoCall} = this.props;
        const {isOpenModalWindow, acceptorCallback, isFull} = this.state;
        let enableSelfVideoClass = isReadyForVideoCall ? ' ': ' hide';
        return (
            <div className="ibox ">
                <div className="ibox-content">
                    <PatientAcceptCallModalWindow isOpen={isOpenModalWindow}
                                                  acceptorCallback={acceptorCallback}
                                                  onSelectOption={this.handleAccept}/>
                    <Fullscreen
                        enabled={isFull}
                        onChange={isFull => this.setState({isFull})}>
                        <figure id='video-main' className="full-screenable-node">
                            <video autoPlay="autoplay" className={"easyrtcMirror selfVideo " + enableSelfVideoClass}
                                   id="selfVideo" muted="muted" volume="0"/>
                            <video autoPlay="autoplay" id="callerVideo"/>
                            <VideoWindowIcons setFullWindow={this.goFull} callState={isReadyForVideoCall} />
                        </figure>
                    </Fullscreen>
                </div>
            </div>
        )
    }
}


const mapStateToProps = (state) => ({
    username: reducers.userLogin(state),
    isReadyForVideoCall: reducers.isUserReadyForVideoCall(state)
});

export default connect(mapStateToProps, null)(PatientVideoWindow);