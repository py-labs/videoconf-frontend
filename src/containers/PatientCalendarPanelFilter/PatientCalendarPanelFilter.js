import React, {Component} from 'react';
import {connect} from "react-redux";
import Select from 'react-select';
import DateRangePicker from 'react-bootstrap-daterangepicker';
import 'bootstrap-daterangepicker/daterangepicker.css';

import {serverDoctorPlaceList, serverDoctorSpecializationList, serverSpecializationList} from "../../store/reducers";
import {
    requestDoctorPlaceList,
    requestDoctorSpecializationList,
    requestSpecializationList,
    SET_SELECTED_CALENDAR_FILTERS
} from "../../store/patient/actions";

import './PatientCalendarPanelFilter.css'
import {Button} from "reactstrap";
import moment from 'moment';
import 'moment/locale/ru'

class PatientCalendarPanelFilter extends Component {
    constructor(props) {
        super(props);

        this.state = {
            selectedSpecialization: null,
            selectedDoctor: null,
            selectedPlace: null,
            selectedDateStart: '',
            selectedDateEnd: ''
        };
        this.baseState = this.state;
        this.clearFilter = this.clearFilter.bind(this);
    }

    componentDidMount() {
        this.props.requestSpecializationList();
        this.props.requestDoctorPlaceList();
    }

    getDoctorsSpecialization(selectedOption) {
        this.props.requestDoctorSpecializationList(selectedOption.value);
    }

    setNullValueForSelect(value) {
        if (Array.isArray(value) && value.length === 0)
            value = null;
        return value
    }

    setSelectedSpecialization = (selectedSpecialization) => {
        selectedSpecialization = this.setNullValueForSelect(selectedSpecialization);

        this.setState({selectedSpecialization});

        if (selectedSpecialization)
            this.getDoctorsSpecialization(selectedSpecialization);
        else {
            this.setState({
                selectedDoctor: null
            })
        }
    };

    setSelectedDoctor = (selectedDoctor) => {
        selectedDoctor = this.setNullValueForSelect(selectedDoctor);
        this.setState({selectedDoctor});
    };

    setSelectedPlace = (selectedPlace) => {
        selectedPlace = this.setNullValueForSelect(selectedPlace);
        this.setState({selectedPlace});
    };

    getMessageNoOptions = () => {
        return 'Не найдено'
    };

    saveSelectedFilters = () => {
        this.props.dispatch({
            type: SET_SELECTED_CALENDAR_FILTERS,
            payload: {
                specialization: this.state.selectedSpecialization.value,
                doctor: this.state.selectedDoctor !== null ? this.state.selectedDoctor.value : '',
                place: this.state.selectedPlace !== null ? this.state.selectedPlace.value : '',
                dateStart: this.state.selectedDateStart,
                dateEnd: this.state.selectedDateEnd
            }
        })
    };

    setSelectedDates = (e, picker) => {
        this.setState({
            selectedDateStart: picker.startDate.format('DD.MM.YYYY'),
            selectedDateEnd: picker.endDate.format('DD.MM.YYYY')
        });

    };
    clearSelectedDates = (e, picker) => {
        this.setState({
            selectedDateStart: '',
            selectedDateEnd: ''
        })
    };

    clearFilter = (state) => {
        this.setState(state)
    };

    render() {
        const {specializationList, doctorsSpecialization, doctorPlaces} = this.props;
        const {selectedSpecialization, selectedDoctor, selectedPlace} = this.state;
        const isButtonShowDisabled = !selectedSpecialization;
        const locale = {
            applyLabel: 'Выбрать',
            cancelLabel: 'Очистить'
        };

        moment.locale('ru');

        return (
            <div className="calendar-filter">
                <div className="ibox">
                    <div className="ibox-title">
                        <h5>Выберите специалиста, место и даты приема</h5>
                    </div>
                    <div className="ibox-content">
                        <div className="row">
                            <div className="col-xs-11">
                                <Select
                                    value={selectedSpecialization}
                                    onChange={this.setSelectedSpecialization}
                                    options={specializationList}
                                    className='react-select-container m-b'
                                    classNamePrefix="react-select"
                                    placeholder='Выберите специализацию'
                                    noOptionsMessage={this.getMessageNoOptions}
                                />
                            </div>
                            <div className="col-xs-1 clean-field">
                                <Button color="primary" className="clean-button"
                                        onClick={() => this.clearFilter({selectedSpecialization: null})}>
                                    <i className="fa fa-times" aria-hidden="true"/>
                                </Button>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-xs-11">
                                <Select
                                    isDisabled={!selectedSpecialization}
                                    value={selectedDoctor}
                                    onChange={this.setSelectedDoctor}
                                    options={doctorsSpecialization}
                                    className='react-select-container m-b'
                                    classNamePrefix="react-select"
                                    placeholder='Выберите доктора'
                                    noOptionsMessage={this.getMessageNoOptions}
                                />
                            </div>
                            <div className="col-xs-1 clean-field">
                                <Button color="primary" className="clean-button"
                                        onClick={() => this.clearFilter({selectedDoctor: null})}>
                                    <i className="fa fa-times" aria-hidden="true"/>
                                </Button>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-xs-11">
                                <Select
                                    value={selectedPlace}
                                    onChange={this.setSelectedPlace}
                                    options={doctorPlaces}
                                    className='react-select-container m-b'
                                    classNamePrefix="react-select"
                                    placeholder='Выберите место'
                                    noOptionsMessage={this.getMessageNoOptions}
                                />
                            </div>
                            <div className="col-xs-1 clean-field">
                                <Button color="primary" className="clean-button"
                                        onClick={() => this.clearFilter({selectedPlace: null})}>
                                    <i className="fa fa-times" aria-hidden="true"/>
                                </Button>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-xs-11">
                                <DateRangePicker
                                    onApply={this.setSelectedDates}
                                    locale={locale}
                                    onCancel={this.clearSelectedDates}>
                                    <input name="datefilter" className="form-control m-b"
                                           placeholder="Выберите даты приема"
                                           defaultValue={this.state.selectedDateStart ? this.state.selectedDateStart + '-' + this.state.selectedDateEnd : ''}/>
                                </DateRangePicker>
                            </div>
                            <div className="col-xs-1 clean-field">
                                <Button color="primary" className="clean-button"
                                        onClick={() => this.clearFilter({selectedDateStart: null})}>
                                    <i className="fa fa-times" aria-hidden="true"/>
                                </Button>
                            </div>
                        </div>
                        <Button color="primary" className="filter-button"
                                disabled={isButtonShowDisabled}
                                onClick={this.saveSelectedFilters}>
                            Посмотреть свободные даты
                        </Button>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    specializationList: serverSpecializationList(state),
    doctorsSpecialization: serverDoctorSpecializationList(state),
    doctorPlaces: serverDoctorPlaceList(state)

});

const mapDispatchToProps = (dispatch) => ({
    requestSpecializationList: (event) => dispatch(requestSpecializationList(event)),
    requestDoctorSpecializationList: (event) => dispatch(requestDoctorSpecializationList(event)),
    requestDoctorPlaceList: (event) => dispatch(requestDoctorPlaceList(event)),
    dispatch: dispatch,
});


export default connect(mapStateToProps, mapDispatchToProps)(PatientCalendarPanelFilter);
