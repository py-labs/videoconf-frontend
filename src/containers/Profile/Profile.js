import React, {Component} from 'react'
import ProfileList from "../../components/ProfileList/ProfileList"
import ProfileHeader from "../../components/ProfileHeader/ProfileHeader"

class Profile extends Component {
    render() {
        return (
            <div className="row">
                <div className="col-lg-12">
                    <div className="wrapper wrapper-content {/*animated fadeInUp*/}">
                        {/*<ProfileHeader />*/}
                        <ProfileList />
                    </div>
                </div>
            </div>

        )
    }
}

export default Profile;