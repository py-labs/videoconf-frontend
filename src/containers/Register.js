import React from 'react'
import {connect} from 'react-redux'
import {Redirect} from 'react-router'

import RegisterForm from '../components/RegisterForm'
import {isAuthenticated} from '../store/reducers'

const Register = (props) => {
    if (props.isAuthenticated) {
        return (
            <Redirect to='/'/>
        )
    } else {
        return (
            <div className="login-page">
                <RegisterForm {...props}/>
            </div>

        )
    }
}

const mapStateToProps = (state) => ({
    isAuthenticated: isAuthenticated(state)
})

export default connect(mapStateToProps)(Register);