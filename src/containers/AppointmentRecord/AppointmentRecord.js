import React, {Component} from 'react';
import './AppointmentRecord.css'
import PatientCalendar from "../PatientCalendar/PatientCalendar";
import PatientCalendarPanelFilter from "../PatientCalendarPanelFilter/PatientCalendarPanelFilter";
import PatientSelectedTimeRecord from "../PatientSelectedTimeRecord/PatientSelectedTimeRecord";


class AppointmentRecord extends Component {
    state = {
        date: new Date()
    }

    render() {
        return (
            <div className="row">
                <div className="col-xs-12">
                    <div className="row">
                        <div className="col-md-6">
                            <PatientCalendarPanelFilter/>
                        </div>
                        <div className="col-md-6">
                            {/*<h3>&nbsp;</h3>*/}
                            <PatientSelectedTimeRecord />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-xs-12">
                            <div className="wrapper wrapper-content">
                                <PatientCalendar value={this.state.date}/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}


export default AppointmentRecord;