import React, {Component} from 'react';
import {connect} from 'react-redux';
import {patientTimeRecord, getPatientProfile} from "../../store/reducers";
import './AvailableTime.css'
import {requestCreateAppointment} from "../../store/appointments/actions";
import {Button} from "reactstrap";

class PatientSelectedTimeRecord extends Component {
    state = {
        isRecorded: false
    };

    setRecorded(state){
        this.setState({
            isRecorded: state
        })
    }
    componentDidUpdate(prevProps) {
        // Typical usage (don't forget to compare props):
        if (this.props.timeRecord !== prevProps.timeRecord) {
            this.setRecorded(false)
        }
    }

    createPatientAppointment = () => {
        const {patient, timeRecord} = this.props;
        this.props.createAppointment(timeRecord.doctor.pk, patient.pk,
            timeRecord.date, timeRecord.time.id, timeRecord.place.id);

        this.setRecorded(true);

    };

    isEmpty = (obj) => {
      return Object.keys(obj).length === 0;
    };

    getSelectedTime = () => {
        const {timeRecord} = this.props;
        const {isRecorded} = this.state;
        const dd = new Date(timeRecord.date);

        if (!this.isEmpty(timeRecord) && !isRecorded) {
            const key = timeRecord.date + '-' + timeRecord.time.id + '-' + timeRecord.doctor.pk + '-' + timeRecord.place.id;

            return (
                <div className="row" key={key}>
                    {/*<div className="col-xs-12"><u>Выбранное время:</u></div>*/}
                    <div className="col-xs-12 m-b">Дата: {dd.getDate() + '.' + (dd.getMonth() + 1) + '.' + dd.getFullYear()}</div>
                    <div className="col-xs-12 m-b">Время: {timeRecord.time.time}</div>
                    <div className="col-xs-12 m-b">Доктор: {timeRecord.doctor.name}</div>
                    <div className="col-xs-12 m-b">Место: {timeRecord.place.name}</div>
                    <div className="col-xs-12 m-b">
                        <Button color="primary" onClick={this.createPatientAppointment.bind(this)}>Записаться</Button>
                    </div>
                </div>
            )
        }
        else{
            return (
                <div className="row">Время приема пока не выбрано.</div>
            )
        }
    };

    render() {
        return (
            <div className="ibox">
                <div className="ibox-title">
                    <h5>Выбранное время</h5>
                </div>
                <div className="ibox-content">
                    {this.getSelectedTime()}
                </div>
            </div>
        )
    }

}

const mapStateToProps = (state) => ({
    timeRecord: patientTimeRecord(state),
    patient: getPatientProfile(state),
});

const mapDispatchToProps = {
    createAppointment: requestCreateAppointment
};


export default connect(mapStateToProps, mapDispatchToProps)(PatientSelectedTimeRecord);