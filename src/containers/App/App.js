import React, {Component} from 'react';
import {connect} from 'react-redux';

import * as reducers from '../../store/reducers'
import AppRouter from "./AppRouter";
import SideBar from "../../components/SideBar/SideBar";
import Footer from '../../components/common/Footer';
import TopHeader from '../../components/common/TopHeader';
import PageHeader from '../../components/common/PageHeader';
// import './App.css'


export class App extends Component {
    render() {
        const {url} = this.props.match;
        return (
            <div /*className="wrapper"*/ id="wrapper">
                <SideBar url={url}/>
                <div id="page-wrapper" className="gray-bg">
                    <TopHeader />
                    {/*<PageHeader pagehead={"Личный кабинет медицинской клиники"} />*/}
                    <AppRouter url={url}/>
                    <Footer />
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    isAuthenticated: reducers.isAuthenticated(state),
    userLogin: reducers.userLogin(state)

});

export default connect(mapStateToProps, null)(App);
