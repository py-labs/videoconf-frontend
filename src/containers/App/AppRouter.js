import React from 'react';
import {Route, Switch} from 'react-router-dom';
import asyncComponent from '../../helpers/AsyncFunc';


class AppRouter extends React.Component {
    render() {
        const {url} = this.props;
        return (
            <Switch>
                <Route
                    exact
                    path='/'
                    component={asyncComponent(() => import('../Workspace/Workspace'))}
                />
                <Route
                    exact
                    path={`${url}appoint`}
                    component={asyncComponent(() => import('../Appoint/Appoint'))}
                />
                <Route
                    exact
                    path={`${url}appoint/detail`}
                    component={asyncComponent(() => import('../../components/DoctorMedcartDetail/DoctorMedcartDetail'))}
                />
                <Route
                    exact
                    path={`${url}conf`}
                    component={asyncComponent(() => import('../Conference/Conference'))}
                />
                <Route
                    exact
                    path={`${url}appointments`}
                    component={asyncComponent(() => import('../../components/PatientAppointmetnsWindow/PatientAppointmetnsWindow'))}
                />
                <Route
                    exact
                    path={`${url}medcart`}
                    component={asyncComponent(() => import('../Medcart/Medcart'))}
                />
                <Route
                    exact
                    path={`${url}medcart/detail`}
                    component={asyncComponent(() => import('../../components/MedcartDetail/MedcartDetail'))}
                />
                <Route
                    exact
                    path={`${url}medcart/analysis`}
                    component={asyncComponent(() => import('../../components/MedcartAnalysis/MedcartAnalysis'))}
                />
                <Route
                    exact
                    path={`${url}files`}
                    component={asyncComponent(() => import('../../components/MedcartFiles/MedcartFiles'))}
                />
                <Route
                    exact
                    path={`${url}profile`}
                    component={asyncComponent(() => import('../Profile/Profile'))}
                />
                <Route
                    exact
                    path={`${url}record`}
                    component={asyncComponent(() => import('../AppointmentRecord/AppointmentRecord'))}
                />
                <Route
                    path={`${url}options`}
                    component={asyncComponent(() => import('../Options/Options'))}
                />
                <Route
                    path={`${url}events`}
                    component={asyncComponent(() => import('../Events/Events'))}
                />
            </Switch>
        );
    }
}

export default AppRouter;
