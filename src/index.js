import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './containers/App/App';
import registerServiceWorker from './registerServiceWorker';
import createHistory from 'history/createBrowserHistory';
import {ConnectedRouter} from 'react-router-redux';
import {Provider} from 'react-redux';

import {Route, Switch} from 'react-router'
import Login from './containers/Login'
import Register from './containers/Register'
import Forget from './containers/Forget'
import PrivateRoute from './containers/PrivateRoute';

// import 'bootstrap/dist/css/bootstrap.css';
import configureStore from './store/store';
import asyncComponent from "./helpers/AsyncFunc";

const history = createHistory();
const store = configureStore(history);


ReactDOM.render((
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <Switch>
                <Route exact path="/register/" component={Register} />
                <Route exact path="/forget/" component={Forget} />
                <Route exact path="/login/" component={Login}/>
                <PrivateRoute path="/" component={App} />
            </Switch>
        </ConnectedRouter>
    </Provider>
), document.getElementById('root'));
registerServiceWorker();
