import React, {Component} from 'react'
import {Link} from "react-router-dom";
import * as reducers from "../../store/reducers";
import connect from "react-redux/es/connect/connect";

class ProfileHeader extends Component {
    render() {
        const {username} = this.props;

        return (
            <div className="row m-b-lg m-t-lg">
                <div className="col-md-6">
                    <div className="profile-image">
                        <img src="img/usrdefault1.png" className="rounded-circle circle-border m-b-md" alt="profile image" />
                    </div>
                    <div className="profile-info">
                        <div className="">
                            <div>
                                <h2 className="no-margins">
                                    {username}
                                </h2>
                                <p>
                                    Дополнительная информация профиля.
                                </p>
                                <small>
                                    Изменить фото
                                </small>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-md-3">
                    <div className="row m-t-lg">
                        <div className="col-md-4">
                            <span className="bar" style={{display: 'none'}}>5,3,9,6,5,9,7,3,5,2</span>
                            <svg className="peity" height="16" width="32">
                                <rect fill="#1ab394" x="0" y="7.111111111111111" width="2.3"
                                      height="8.88888888888889"/>
                                <rect fill="#d7d7d7" x="3.3" y="10.666666666666668" width="2.3"
                                      height="5.333333333333333"/>
                                <rect fill="#1ab394" x="6.6" y="0" width="2.3" height="16"/>
                                <rect fill="#d7d7d7" x="9.899999999999999" y="5.333333333333334" width="2.3"
                                      height="10.666666666666666"/>
                                <rect fill="#1ab394" x="13.2" y="7.111111111111111" width="2.3"
                                      height="8.88888888888889"/>
                                <rect fill="#d7d7d7" x="16.5" y="0" width="2.3" height="16"/>
                                <rect fill="#1ab394" x="19.799999999999997" y="3.555555555555557" width="2.3"
                                      height="12.444444444444443"/>
                                <rect fill="#d7d7d7" x="23.099999999999998" y="10.666666666666668" width="2.3"
                                      height="5.333333333333333"/>
                                <rect fill="#1ab394" x="26.4" y="7.111111111111111" width="2.3"
                                      height="8.88888888888889"/>
                                <rect fill="#d7d7d7" x="29.7" y="12.444444444444445" width="2.3"
                                      height="3.5555555555555554"/>
                            </svg>
                            <h5><strong>XX</strong> инфо</h5>
                        </div>
                        <div className="col-md-4">
                            <span className="line" style={{display: 'none'}}>5,3,9,6,5,9,7,3,5,2</span>
                            <svg className="peity" height="16" width="32">
                                <polygon fill="#1ab394"
                                         points="0 15 0 7.166666666666666 3.5555555555555554 10.5 7.111111111111111 0.5 10.666666666666666 5.5 14.222222222222221 7.166666666666666 17.77777777777778 0.5 21.333333333333332 3.833333333333332 24.888888888888886 10.5 28.444444444444443 7.166666666666666 32 12.166666666666666 32 15"/>
                                <polyline fill="transparent"
                                          points="0 7.166666666666666 3.5555555555555554 10.5 7.111111111111111 0.5 10.666666666666666 5.5 14.222222222222221 7.166666666666666 17.77777777777778 0.5 21.333333333333332 3.833333333333332 24.888888888888886 10.5 28.444444444444443 7.166666666666666 32 12.166666666666666"
                                          stroke="#169c81" stroke-width="1" stroke-linecap="square"/>
                            </svg>
                            <h5><strong>XX</strong> инфо</h5>
                        </div>
                        <div className="col-md-4">
                            <span className="bar" style={{display: 'none'}}>5,3,2,-1,-3,-2,2,3,5,2</span>
                            <svg className="peity" height="16" width="32">
                                <rect fill="#1ab394" x="0" y="0" width="2.3" height="10"/>
                                <rect fill="#d7d7d7" x="3.3" y="4" width="2.3" height="6"/>
                                <rect fill="#1ab394" x="6.6" y="6" width="2.3" height="4"/>
                                <rect fill="#d7d7d7" x="9.899999999999999" y="10" width="2.3" height="2"/>
                                <rect fill="#1ab394" x="13.2" y="10" width="2.3" height="6"/>
                                <rect fill="#d7d7d7" x="16.5" y="10" width="2.3" height="4"/>
                                <rect fill="#1ab394" x="19.799999999999997" y="6" width="2.3" height="4"/>
                                <rect fill="#d7d7d7" x="23.099999999999998" y="4" width="2.3" height="6"/>
                                <rect fill="#1ab394" x="26.4" y="0" width="2.3" height="10"/>
                                <rect fill="#d7d7d7" x="29.7" y="6" width="2.3" height="4"/>
                            </svg>
                            <h5><strong>XX</strong> инфо</h5>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    username: reducers.getUserFullName(state)
});

export default connect(mapStateToProps, null)(ProfileHeader);
