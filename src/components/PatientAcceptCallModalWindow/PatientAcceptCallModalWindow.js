import React, {Component} from 'react'
import {Button, Modal, ModalBody, ModalFooter, ModalHeader} from 'reactstrap';


class PatientAcceptCallModalWindow extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modal: props.isOpen,
            acceptorCallback: props.acceptorCallback
        };

        this.toggle = this.toggle.bind(this);
    }

    toggle(isAccept) {
        this.setState({
            modal: !this.state.modal
        });
        this.props.onSelectOption(false);
        const {acceptorCallback} = this.props;
        if (acceptorCallback)
            acceptorCallback(isAccept);
    }

    componentDidUpdate(prevProps) {
        // Typical usage (don't forget to compare props):
        if (this.props.isOpen !== prevProps.isOpen) {
            this.setState({
                modal: this.props.isOpen,
                acceptorCallback: this.props.acceptorCallback
            })
        }
    }

    render() {
        const {modal} = this.state;
        return (
            <div>
                <Modal isOpen={modal} className="show modal-widow">
                    <ModalHeader>Входящий звонок</ModalHeader>
                    <ModalBody>
                        Вам поступил входящий звонок от доктора. Если готовы к приему, нажмите кнопку Разрешить или Отклонить в противном случае.
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={() => this.toggle(true)}>Разрешить</Button>{' '}
                        <Button color="secondary" onClick={() => this.toggle(false)}>Отклонить</Button>
                    </ModalFooter>
                </Modal>
            </div>
        )
    }
}

export default PatientAcceptCallModalWindow;
