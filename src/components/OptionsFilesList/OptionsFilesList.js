import React, {Component} from 'react'
import {Link} from "react-router-dom";

class RelativeProfiles extends Component {
    render() {
        return (
            <div className="ibox">
                <div className="ibox-content">
                    <div className="project-list">
                        <table className="table table-hover">

                            <thead>
                            <tr>
                                <th style={{width: '5%'}}>
                                </th>
                                <th>
                                    Название файла
                                </th>
                                <th style={{width: '15%'}}>
                                    Дата загрузки
                                </th>
                                <th style={{width: '5%'}}>
                                    Удалить
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td className="project-people">
                                    <img alt="image" className="rounded-circle" src="img/if_jpg.png" />
                                </td>
                                <td className="project-title">
                                    <span>Название файла</span>
                                </td>
                                <td className="project-title">
                                    <span>Дата загрузки</span>
                                </td>
                                <td className="project-actions">
                                    <Link to="" className="btn btn-white btn-sm">
                                        <i className="fa fa-trash" />
                                    </Link>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    {/*<button className="btn btn-white btn-sm" type="submit">Отмена</button>*/}
                    <button className="btn btn-primary" type="submit">Добавить файл</button>
                </div>
            </div>
        )
    }
}

export default RelativeProfiles;