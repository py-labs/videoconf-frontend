import React, {Component} from 'react';
import {connect} from 'react-redux';
import {getUserFullName, isUserDoctor, selectedAppointmentDoctorFullName} from '../../store/reducers'

class DoctorDescriptionPanel extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isCollapsed: true,
        }
    }


    render() {
        const {userFullName, isDoctor, doctorFullName} = this.props;
        const doctor = isDoctor ? userFullName : doctorFullName;


        return (
            <div className={"ibox" + (this.state.isCollapsed ? ' collapsed' : '' )}>
                <div className="ibox-title">
                    <h5>Прием ведет: <strong>{doctor}</strong></h5>
                    <div className="ibox-tools">
                        <a className="collapse-link"
                           onClick={() => this.setState({isCollapsed: !this.state.isCollapsed})}>
                            <i className="fa fa-chevron-up"/>
                        </a>
                    </div>
                </div>
                <div className="ibox-content">
                    <div className="row">
                        <div className="col-md-6">

                            <div className="profile-image">
                                <img src="img/doctor.png" className="rounded-circle circle-border m-b-md"
                                     alt="profile"/>
                            </div>
                            <div className="profile-info">
                                <div className="">
                                    <div>
                                        <h2 className="no-margins">
                                            {doctor}
                                        </h2>
                                        <h4>Врач-гастроэнтеролог</h4>
                                        <p>
                                            Консультативный прием
                                            пациентов(взрослых и детей) с патологией ротовой полости, пищевода, желудка,
                                            печени, поджелудочной железы, кишечника, пациентов с нарушением процессов
                                            пищеварения на фоне антибиотиков, лучевой, химиотерапии.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        )
    }
}


const mapStateToProps = (state) => ({
    isDoctor: isUserDoctor(state),
    doctorFullName: selectedAppointmentDoctorFullName(state),
    userFullName: getUserFullName(state)
})


export default connect(mapStateToProps, null)(DoctorDescriptionPanel);