import React from 'react';
// import { Dropdown } from 'react-bootstrap';

class PageHeader extends React.Component {
    render() {

        const pagehead = this.props.pagehead;

        return (

            <div className="row wrapper border-bottom white-bg page-heading">
                <div className="col-lg-10">
                    <h2>{pagehead}</h2>
{/*
                    <ol className="breadcrumb">
                        <li className="breadcrumb-item">
                            <a href="/">Главная</a>
                        </li>
                        <li className="breadcrumb-item active">
                            <strong>Chat view</strong>
                        </li>
                    </ol>
*/}
                </div>
                <div className="col-lg-2">

                </div>
            </div>
        )
    }
}

export default PageHeader