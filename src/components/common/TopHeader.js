import React from 'react';
// import { Dropdown } from 'react-bootstrap';
import {smoothlyMenu} from '../../helpers/Helpers';
import $ from 'jquery';
import {logout} from "../../store/auth/actions";
import * as reducers from '../../store/reducers'
import {connect} from "react-redux";
import Clock from "../Clock/Clock"
import CitySelect from "../CitySelect/CitySelect"

class TopHeader extends React.Component {

    toggleNavigation(e) {
        e.preventDefault();
        $("body").toggleClass("mini-navbar");
        smoothlyMenu();
    }

    toggleRightSidebar(e) {
        e.preventDefault();
        $("div.right-sidebar").toggleClass("sidebar-open");
    }

    logout(e) {
        e.preventDefault();
        this.props.dispatch(logout())
    }

    render() {
        return (
            <div className="row border-bottom">
                <nav className="navbar navbar-static-top white-bg" role="navigation" style={{marginBottom: 0}}>
                    <div className="navbar-header">
                        <a className="navbar-minimalize minimalize-styl-2 btn btn-primary "
                           onClick={this.toggleNavigation} href="#"><i className="fa fa-bars"></i> </a>
                    </div>
                    <ul className="nav navbar-top-links navbar-right">
                        <li className="dropdown">
                            <CitySelect />
                        </li>
                        <li>
                            <Clock />
                        </li>
                        <li>
                            <a href="" onClick={this.logout.bind(this)}>
                                <i className="fa fa-sign-out"></i> Выход
                            </a>
                        </li>
                        <li>
                            <a className="right-sidebar-toggle" onClick={this.toggleRightSidebar}>
                                <i className="fa fa-tasks"></i>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    isAuthenticated: reducers.isAuthenticated(state),
    username: reducers.userLogin(state)

});

export default connect(mapStateToProps, null)(TopHeader);
