import React from 'react';

class Footer extends React.Component {
    render() {
        return (
            <div className="footer">
                <div className="pull-right">

                </div>
                <div>
                    <strong>Copyright</strong> MedRTC &copy; 2018
                </div>
            </div>
        )
    }
}

export default Footer