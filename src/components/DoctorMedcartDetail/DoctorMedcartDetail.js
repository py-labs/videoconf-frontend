import React, {Component} from 'react'
import {Link} from "react-router-dom";
import { Document, Page } from 'react-pdf';

class DoctorMedcartDetail extends Component {
    state = {
        numPages: null,
        pageNumber: 1,
    }

    onDocumentLoad = ({ numPages }) => {
        this.setState({ numPages });
    }

    render() {
        const { pageNumber, numPages } = this.state;

        return (
            <div className="row">
                <div className="col-lg-12">
                    <div className="wrapper wrapper-content {/*animated fadeInUp*/}">
                        <div className="ibox">

                            <div className="ibox-content">
                                <div className="m-b-md">
                                    <Link to="/appoint" className="btn btn-primary btn-xs float-right">
                                        К списку
                                    </Link>
                                    <h3>Данные по приему <i>ФИО пациента</i> <i>Дата приема</i></h3>
                                </div>
                                <Document
                                    file="/img/f025y-04.pdf"
                                    loading="Загружается файл..."
                                    onLoadSuccess={this.onDocumentLoad}
                                >
                                    <Page pageNumber={pageNumber} width="1000"/>
                                </Document>
                                <button onClick={() => this.setState(prevState => ({ pageNumber: prevState.pageNumber - 1 }))}>
                                    Пред
                                </button>
                                <span>Page {pageNumber} of {numPages}</span>
                                <button onClick={() => this.setState(prevState => ({ pageNumber: prevState.pageNumber + 1 }))}>
                                    След
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default DoctorMedcartDetail;
