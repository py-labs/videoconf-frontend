import React, {Component} from 'react'
import {Link} from "react-router-dom";
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import DatePicker from 'react-datepicker';
import RelativeProfiles from "../RelativeProfiles/RelativeProfiles"
import OptionsFilesList from "../OptionsFilesList/OptionsFilesList"
import moment from 'moment';
import 'moment/locale/ru';

import 'react-datepicker/dist/react-datepicker.css';
import './OptionsList.css';

class ProfileList extends Component {
    constructor () {
        super()
        this.state = {
            //startDate: moment()
            startDate: '01.01.2018'
        };
    }

    handleChange = (date) => {
        this.setState({
            startDate: date
        });
    }

    render() {

        moment.locale('ru');

        return (
            <div className="row">
                <div className="col-lg-12">
                    <Tabs className="tabs-container">
                        <TabList className="nav nav-tabs">
                            <Tab><a className="nav-link" >Персональные данные</a></Tab>
                            <Tab><a className="nav-link" >Медицинские данные</a></Tab>
                            <Tab><a className="nav-link" >Безопасность</a></Tab>
                            <Tab><a className="nav-link" >Семейный доступ</a></Tab>
                            <Tab><a className="nav-link" >Файлы</a></Tab>
                        </TabList>
                        <TabPanel>
                            <form method="get">
                                <div className="panel-body">
                                    <fieldset>
                                    <div className="form-group row">
                                        <label className="col-sm-1 col-form-label text-center">Фамилия:</label>
                                        <div className="col-sm-3">
                                            <input type="text" disabled className="form-control" placeholder="Иванов" />
                                        </div>
                                        <label className="col-sm-1 col-form-label text-center">Имя:</label>
                                        <div className="col-sm-3">
                                            <input type="text" disabled className="form-control" placeholder="Иван" />
                                        </div>
                                        <label className="col-sm-1 col-form-label text-center">Отчество:</label>
                                        <div className="col-sm-3">
                                            <input type="text" disabled className="form-control" placeholder="Иванович" />
                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <label className="col-sm-1 col-form-label text-center">Номер телефона:</label>
                                        <div className="col-sm-3">
                                            <div className="input-group select">
                                                <div className="input-group-addon">
                                                    <i className="fa fa-pencil"/>
                                                </div>
                                                <input type="text" disabled className="form-control" placeholder="+79000000000" />
                                            </div>
                                        </div>
                                        <label className="col-sm-1 col-form-label text-center">E-mail:</label>
                                        <div className="col-sm-3">
                                            <div className="input-group select">
                                                <div className="input-group-addon">
                                                    <i className="fa fa-pencil"/>
                                                </div>
                                                <input type="text" disabled className="form-control" placeholder="ivanov@mail.ru" />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <label className="col-sm-1 col-form-label text-center">Пол:</label>
                                        <div className="col-sm-3">
                                            <select className="form-control">
                                                <option>М</option>
                                                <option>Ж</option>
                                            </select>
                                        </div>
                                        <label className="col-sm-1 col-form-label text-center">Дата рождения:</label>
                                        <div className="col-sm-3">
                                            <div className="input-group date">
                                                <span className="input-group-addon"><i className="fa fa-calendar"/></span>
                                                <input type="text" className="form-control" value={this.state.startDate} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="hr-line-dashed"></div>
                                    <div className="form-group row">
                                        <label className="col-sm-2 col-form-label">Адрес регистрации:</label>
                                    </div>
                                    <div className="form-group row">
                                        <label className="col-sm-1 col-form-label text-center">  Субъект РФ:</label>
                                        <div className="col-sm-3">
                                            <div className="input-group select">
                                                <div className="input-group-addon">
                                                    <i className="fa fa-ellipsis-h"/>
                                                </div>
                                                <input type="text" className="form-control" placeholder="Выберите субъект" />
                                            </div>
                                        </div>
                                        <label className="col-sm-1 col-form-label text-center">  Город:</label>
                                        <div className="col-sm-3">
                                            <div className="input-group select">
                                                <div className="input-group-addon">
                                                    <i className="fa fa-ellipsis-h"/>
                                                </div>
                                                <input type="text" className="form-control" placeholder="Выберите город" />
                                            </div>
                                        </div>
                                        <label className="col-sm-1 col-form-label text-center">  Улица:</label>
                                        <div className="col-sm-3">
                                            <div className="input-group select">
                                                <div className="input-group-addon">
                                                    <i className="fa fa-ellipsis-h"/>
                                                </div>
                                                <input type="text" className="form-control" placeholder="Выберите улицу" />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <label className="col-sm-1 col-form-label text-center">Дом:</label>
                                        <div className="col-sm-3">
                                            <input type="text" className="form-control" placeholder="Введите номер дома" />
                                        </div>
                                        <label className="col-sm-1 col-form-label text-center">Квартира:</label>
                                        <div className="col-sm-3">
                                            <input type="text" className="form-control" placeholder="Введите номер квартиры" />
                                        </div>
                                        <label className="col-sm-1 col-form-label text-center">Местность:</label>
                                        <div className="col-sm-3">
                                            <select className="form-control">
                                                <option>Городская</option>
                                                <option>Сельская</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div className="hr-line-dashed"></div>

                                    <div className="form-group row">
                                        <label className="col-sm-2 col-form-label">Полис ОМС:</label>
                                    </div>
                                    <div className="form-group row">
                                        <label className="col-sm-1 col-form-label text-center">Страховая организация:</label>
                                        <div className="col-sm-3">
                                            <div className="input-group select">
                                                <div className="input-group-addon">
                                                    <i className="fa fa-ellipsis-h"/>
                                                </div>
                                                <input type="text" className="form-control" placeholder="Выберите организацию" />
                                            </div>
                                        </div>
                                        <label className="col-sm-1 col-form-label text-center">Номер:</label>
                                        <div className="col-sm-3">
                                            <input type="text" className="form-control" placeholder="Введите номер полиса" />
                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <label className="col-sm-2 col-form-label">Полис ДМС:</label>
                                    </div>
                                    <div className="form-group row">
                                        <label className="col-sm-1 col-form-label text-center">Страховая организация:</label>
                                        <div className="col-sm-3">
                                            <div className="input-group select">
                                                <div className="input-group-addon">
                                                    <i className="fa fa-ellipsis-h"/>
                                                </div>
                                                <input type="text" className="form-control" placeholder="Выберите организацию" />
                                            </div>
                                        </div>
                                        <label className="col-sm-1 col-form-label text-center">Номер:</label>
                                        <div className="col-sm-3">
                                            <input type="text" className="form-control" placeholder="Введите номер полиса" />
                                        </div>
                                    </div>
                                    <div>
                                        <div className="form-group row">
                                            <label className="col-sm-1 col-form-label text-center">Скан полиса:</label>
                                            <div className="col-sm-3">
                                                <a className="btn btn-primary" href="">Загрузить</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="hr-line-dashed"></div>
                                    <div className="form-group row">
                                        <label className="col-sm-1 col-form-label text-center">СНИЛС:</label>
                                        <div className="col-sm-3">
                                            <input type="text" className="form-control" placeholder="Введите номер СНИЛС" />
                                        </div>
                                        <label className="col-sm-1 col-form-label text-center">Код категории льготы:</label>
                                        <div className="col-sm-3">
                                            <div className="input-group select">
                                                <div className="input-group-addon">
                                                    <i className="fa fa-ellipsis-h"/>
                                                </div>
                                                <input type="text" className="form-control" placeholder="Выберите категорию" />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="hr-line-dashed"></div>
                                    <div className="form-group row">
                                        <label className="col-sm-2 col-form-label">Документ:</label>
                                    </div>
                                    <div className="form-group row">
                                        <label className="col-sm-1 col-form-label text-center">Вид документа:</label>
                                        <div className="col-sm-3">
                                            <select className="form-control">
                                                <option>Паспорт РФ</option>
                                                <option>Загранпаспорт</option>
                                                <option>...</option>
                                            </select>
                                        </div>
                                        <label className="col-sm-1 col-form-label text-center">Серия:</label>
                                        <div className="col-sm-3">
                                            <input type="text" className="form-control" placeholder="Введите серию документа" />
                                        </div>

                                        <label className="col-sm-1 col-form-label text-center">Номер:</label>
                                        <div className="col-sm-3">
                                            <input type="text" className="form-control" placeholder="Введите номер документа" />
                                        </div>
                                    </div>
                                    <div className="hr-line-dashed"></div>
                                    <div className="form-group row">
                                        <label className="col-sm-1 col-form-label text-center">Семейное положение:</label>
                                        <div className="col-sm-3">
                                            <select className="form-control">
                                                <option>Состою в зарегистрированном браке</option>
                                                <option>Не состою в браке</option>
                                                <option>Неизвестно</option>
                                            </select>
                                        </div>
                                        <label className="col-sm-1 col-form-label text-center">Образование:</label>
                                        <div className="col-sm-3">
                                            <select className="form-control">
                                                <option>Высшеее</option>
                                                <option>Среднее</option>
                                                <option>Общее среднее</option>
                                                <option>Основное</option>
                                                <option>Начальное</option>
                                                <option>Неизвестно</option>
                                            </select>
                                        </div>
                                        <label className="col-sm-1 col-form-label text-center">Занятость:</label>
                                        <div className="col-sm-3">
                                            <select className="form-control">
                                                <option>Работаю</option>
                                                <option>Прохожу военную служба</option>
                                                <option>Пенсионер</option>
                                                <option>Студент</option>
                                                <option>Не работаю</option>
                                                <option>Прочее</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <label className="col-sm-1 col-form-label text-center">Место работы, должность:</label>
                                        <div className="col-sm-11">
                                            <input type="text" className="form-control" placeholder="Введите место работы" />
                                        </div>
                                    </div>
                                    <div className="hr-line-dashed"></div>
                                </fieldset>
                                    <div className="form-group row">
                                        <div className="col-sm-10 col-sm-offset-0">
                                            {/*<button className="btn btn-white btn-sm" type="submit">Отмена</button>*/}
                                            <button className="btn btn-primary" type="submit">Сохранить</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </TabPanel>

                        <TabPanel>
                            <form method="get">
                                <div className="panel-body">
                                    <fieldset>
                                        <div className="form-group row">
                                            <label className="col-sm-2 col-form-label">Инвалидность:</label>
                                        </div>
                                        <div className="form-group row">
                                            <label className="col-sm-1 col-form-label text-center">Тип:</label>
                                            <div className="col-sm-3">
                                                <select className="form-control">
                                                    <option>-</option>
                                                    <option>Первичная</option>
                                                    <option>Вторичная</option>
                                                </select>
                                            </div>
                                            <label className="col-sm-1 col-form-label text-center">Группа:</label>
                                            <div className="col-sm-3">
                                                <select className="form-control">
                                                    <option>-</option>
                                                    <option>I</option>
                                                    <option>II</option>
                                                    <option>III</option>
                                                </select>
                                            </div>
                                            <label className="col-sm-1 col-form-label text-center">Дата:</label>
                                            <div className="col-sm-3">
                                                <div className="input-group date">
                                                    <span className="input-group-addon"><i className="fa fa-calendar"/></span>
                                                    {/*<DatePicker
                                                            className="form-control"
                                                            selected={this.state.startDate}
                                                            onChange={this.handleChange}
                                                            locale="ru"
                                                        />*/}
                                                    <input type="text" className="form-control" value={this.state.startDate} />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="hr-line-dashed"></div>
                                        <div className="form-group row">
                                            <label className="col-sm-1 col-form-label text-center">Группа крови:</label>
                                            <div className="col-sm-3">
                                                <select className="form-control">
                                                    <option>-</option>
                                                    <option>I</option>
                                                    <option>II</option>
                                                    <option>III</option>
                                                    <option>IV</option>
                                                </select>
                                            </div>
                                            <label className="col-sm-1 col-form-label text-center">Rh-фактор:</label>
                                            <div className="col-sm-3">
                                                <select className="form-control">
                                                    <option>Rh+</option>
                                                    <option>Rh-</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div className="form-group row"><label className="col-sm-1 col-form-label">Аллергические реакции:</label>
                                            <div className="col-sm-10"><input type="text" className="form-control" placeholder="Введите данные" /></div>
                                        </div>
                                    </fieldset>
                                    <div className="form-group row">
                                        <div className="col-sm-10 col-sm-offset-0">
                                            {/*<button className="btn btn-white btn-sm" type="submit">Отмена</button>*/}
                                            <button className="btn btn-primary" type="submit">Сохранить</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </TabPanel>

                        <TabPanel>
                            <form method="get">
                                <div className="panel-body">
                                    <fieldset>
                                    <div className="form-group row"><label className="col-sm-2 col-form-label">Смена пароля</label></div>
                                    <div className="form-group row"><label className="col-sm-2 col-form-label">Текущий пароль:</label>
                                        <div className="col-sm-10"><input type="password" className="form-control" placeholder="Введите пароль" /></div>
                                    </div>
                                    <div className="form-group row"><label className="col-sm-2 col-form-label">Новый пароль:</label>
                                        <div className="col-sm-10"><input type="password" className="form-control" placeholder="Введите пароль" /></div>
                                    </div>
                                    <div className="form-group row"><label className="col-sm-2 col-form-label">Повторите пароль:</label>
                                        <div className="col-sm-10"><input type="password" className="form-control" placeholder="Введите пароль" /></div>
                                    </div>
                                </fieldset>
                                    <div className="form-group row">
                                        <div className="col-sm-10 col-sm-offset-0">
                                            {/*<button className="btn btn-white btn-sm" type="submit">Отмена</button>*/}
                                            <button className="btn btn-primary" type="submit">Сохранить</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </TabPanel>

                        <TabPanel>
                            <RelativeProfiles />
                        </TabPanel>

                        <TabPanel>
                            <OptionsFilesList />
                        </TabPanel>

                    </Tabs>

                </div>
            </div>
        )
    }
}

export default ProfileList;