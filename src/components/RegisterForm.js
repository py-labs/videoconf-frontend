import React, {Component} from 'react'
import {Button, Form} from 'reactstrap';

import TextInput from './TextInput'
import {FormGroup, Dropdown, DropdownToggle, DropdownMenu, DropdownItem} from 'reactstrap';

export default class RegisternForm extends Component {
    onSubmit = (event) => {
        event.preventDefault()
    }

    componentDidMount() {
        document.body.classList.add('gray-bg')
    }

    componentWillUnmount() {
        document.body.classList.remove('gray-bg')
    }

    render() {
        const errors = this.props.errors || {}

        return (
            <div className="middle-box text-center animated fadeInDown" style={{maxWidth: "700px"}}>
                <div>
                    <div>
                        <h1 className="logo-name">TELEMED</h1>
                    </div>
                    <h3>Регистрация в сервисе TeleMed</h3>
                    <Form className="m-t" role="form" action="login.html">
                        <div className="row">
                            <div className="col-md-6">
                                <TextInput name="family" label="Фамилия" />
                                <TextInput name="name" label="Имя" />
                                <TextInput name="name" label="Отчество" />
                                <FormGroup>
                                    <select className="form-control m-b" name="sex">
                                        <option value="" disabled selected>Выберите пол</option>
                                        <option value="M">М</option>
                                        <option value="F">Ж</option>
                                    </select>
                                </FormGroup>
                                <TextInput name="pass1" type="password" label="Пароль" />
                                <Button type="submit" className="btn btn-primary block full-width m-b">Зарегистрироваться</Button>
                            </div>
                            <div className="col-md-6">
                                <TextInput name="family" label="Адрес электронной почты" />
                                <div className="row">
                                    <div className="col-md-8">
                                        <TextInput name="name" label="Мобильный телефон" />
                                    </div>
                                    <div className="col-md-4">
                                        <button className="btn btn-sm btn-primary float-right"><small>Получить смс-код</small></button>
                                    </div>
                                </div>
                                <TextInput name="name" label="Код подтверждения" />
                                <FormGroup>
                                    <select className="form-control m-b" name="birthday">
                                        <option value="" disabled selected>Дата рождения</option>
                                    </select>
                                </FormGroup>
                                <TextInput name="pass2" type="password" label="Повторите пароль" />
                                <div className="form-group">
                                    <div className="row">
                                        <div className="col-md-1">
                                            <input id="ch_service" type="checkbox" value="checkbox" />
                                        </div>
                                        <div className="col-md-11">
                                            <label htmlFor="ch_service"><small> Я принимаю условия и соглашение на использование сервиса</small></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <p className="text-muted text-center">
                            <span>Уже зарегистрированы? </span><a className="btn btn-white" href="/login/">Войти</a>
                        </p>

                    </Form>
                </div>
                <div className="row">
                    <div className="col-md-6 text-left">
                        Telemed
                    </div>
                    <div className="col-md-6 text-right">
                        <small>© 2018</small>
                    </div>
                </div>
            </div>

        )
    }
}