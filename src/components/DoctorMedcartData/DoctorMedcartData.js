import React, {Component} from 'react'
import './DoctorMedcartData.css';
import Select from 'react-select';
import 'react-dates/initialize';
//import { DateRangePicker, SingleDatePicker, DayPickerRangeController } from 'react-dates';
import DateRangePicker from 'react-bootstrap-daterangepicker';
import moment from 'moment';
import "moment/locale/ru"
import 'react-dates/lib/css/_datepicker.css';
import {Link} from "react-router-dom";
import 'bootstrap-daterangepicker/daterangepicker.css';


const options = [
    { value: 'приемы', label: 'приемы' },
    { value: 'анализы', label: 'анализы' },
    { value: 'обследования', label: 'обследования' }
];

moment.locale('ru');
moment().format('L');

class DoctorMedcartData extends Component {
    constructor () {
        super()

        this.state = {
            listView: true,
            focusedInput: null,
            startDate: '',
            endDate: ''

        };
    }

    setSelectedDates = (e, picker) => {
        this.setState({
            startDate: picker.startDate.format('DD.MM.YYYY'),
            endDate: picker.endDate.format('DD.MM.YYYY')
        });
    };
    clearSelectedDates = (e, picker) => {
        this.setState({
            startDate: '',
            endDate: ''
        })
    };

    handleViewClick = () => {
        this.setState(prevState => ({ listView: !prevState.listView}))
    }

    render() {

        const locale = {
            applyLabel: 'Выбрать',
            cancelLabel: 'Очистить'
        };
        const listView = this.state.listView

        return(
            <div>
            {listView ? (
                <div className="row" style={{paddingTop: "10px", height: "100vh"}}>
                    <div className="col-md-4">
                        <div className="input-group">
                            <input type="text" placeholder="Поиск по медкарте" className="form-control-sm form-control"/>
                            <span className="input-group-btn">
                                        <button type="button" className="btn btn-sm btn-primary"> Искать
                                        </button>
                                    </span>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <Select className="select2-container" classNamePrefix="react-select"
                                isMulti
                                placeholder="Фильтр по категориям"
                                options={options}
                        />
                    </div>
                    <div className="col-md-4">
                        {/*<DateRangePicker
                                startDate={this.state.startDate} // momentPropTypes.momentObj or null,
                                startDateId="date1" // PropTypes.string.isRequired,
                                endDate={this.state.endDate} // momentPropTypes.momentObj or null,
                                endDateId="date2" // PropTypes.string.isRequired,
                                onDatesChange={({ startDate, endDate }) => this.setState({ startDate, endDate })} // PropTypes.func.isRequired,
                                focusedInput={this.state.focusedInput} // PropTypes.oneOf([START_DATE, END_DATE]) or null,
                                onFocusChange={focusedInput => this.setState({ focusedInput })} // PropTypes.func.isRequired,
                            />*/}
                        <DateRangePicker
                            onApply={this.setSelectedDates}
                            locale={locale}
                            onCancel={this.clearSelectedDates}>
                            <input name="datefilter" className="form-control" placeholder='Фильтр по датам'
                                   value={this.state.startDate ? this.state.startDate + '-' + this.state.endDate : ''}/>
                        </DateRangePicker>
                    </div>
                    <div className="project-list">
                        <table className="table table-hover">
                            <tbody>
                            <tr>
                                <td className="project-status"><span className="label label-primary">Прием</span></td>
                                <td className="project-title">
                                    <a onClick={this.handleViewClick}>
                                        Прием у гастроэнтеролога
                                        <br/>
                                        <small>Дата 14.08.2018</small>
                                    </a>
                                </td>
                                <td className="project-completion">
                                    <small>Текстовое описание</small>
                                </td>
                                <td className="project-people">
                                    <small>Дополнительная информация</small>
                                </td>

                                <td className="project-actions">
                                    <a onClick={this.handleViewClick} className="btn btn-white btn-sm">

                                        <i className="fa fa-folder"/> Просмотр
                                    </a>
                                </td>
                            </tr>
                            {/*<tr>
                                <td className="project-status"><span className="label label-warning">Обсл</span></td>
                                <td className="project-title">
                                    <Link to="/appoint/detail">
                                        Обследование
                                        <br/>
                                        <small>Дата 24.08.2018</small>
                                    </Link>
                                </td>
                                <td className="project-completion">
                                    <small>Текстовое описание</small>
                                </td>
                                <td className="project-people">
                                    <small>Дополнительная информация</small>
                                </td>
                                <td className="project-actions">
                                    <Link to="/appoint/detail" className="btn btn-white btn-sm">
                                        <i className="fa fa-folder"/> Просмотр
                                    </Link>
                                </td>
                            </tr>*/}
                            </tbody>
                        </table>
                    </div>
                </div>
            ):(
                <div className="row" style={{paddingTop: "10px", height: "100vh"}}>
                    <div className="ibox">
                        <div className="ibox-content">
                            <div className="row">
                                <div className="col-lg-12">
                                    <div className="m-b-md">
                                        <a onClick={this.handleViewClick} className="btn btn-primary btn-xs float-right">
                                            Вернуться
                                        </a>
                                        <h2>Прием у гастроэнтеролога 16.02.2017</h2>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-lg-6">
                                    <dl className="row mb-0">
                                        <div className="col-sm-4 text-sm-right">
                                            <dt>Врач:</dt>
                                        </div>
                                        <div className="col-sm-8 text-sm-left">
                                            <dd className="mb-1">Иванова Мария Алексеевна</dd>
                                        </div>
                                    </dl>

                                </div>
                            </div>
                            <div className="feed-activity-list">
                                <div className="feed-element">
                                    <div className="media-body ">

                                        <strong>Диагноз</strong>
                                        <br/>
                                        <div className="well">
                                            Информация о диагнозе
                                        </div>
                                    </div>
                                </div>
                                <div className="feed-element">
                                    <div className="media-body ">
                                        <small className="float-right">
                                        </small>
                                        <strong>Жалобы</strong>
                                        <br/>
                                        <div className="well">
                                            Информация о жалобах
                                        </div>
                                    </div>
                                </div>
                                <div className="feed-element">
                                    <div className="media-body ">
                                        <small className="float-right">
                                        </small>
                                        <strong>Анамнез</strong>
                                        <br/>
                                        <div className="well">
                                            Информация об анамнезе
                                        </div>
                                    </div>
                                </div>
                                <div className="feed-element">
                                    <div className="media-body ">
                                        <small className="float-right">
                                        </small>
                                        <strong>Рекомендации</strong>
                                        <br/>
                                        <div className="well">
                                            Информация о рекомендациях
                                        </div>
                                    </div>
                                </div>
                                <div className="feed-element">
                                    <div className="media-body ">
                                        <small className="float-right">
                                        </small>
                                        <strong>Назначения</strong>
                                        <br/>
                                        <div className="well">
                                            <table className="table table-striped">
                                                <thead>
                                                <tr>
                                                    <th>Препарат</th>
                                                    <th>Дозировка</th>
                                                    <th>Схема приема</th>
                                                    <th>Комментарии</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>
                                                        Название препарата
                                                    </td>
                                                    <td>
                                                        0,25 мг
                                                    </td>
                                                    <td>
                                                        по 1 табл. 3 раза в день после еды
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Название препарата
                                                    </td>
                                                    <td>
                                                        0,5 мг
                                                    </td>
                                                    <td>
                                                        по 1 табл. 1 раза в день после еды
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div className="feed-element">
                                    <div className="media-body ">
                                        <small className="float-right">
                                        </small>
                                        <strong>Файлы</strong>
                                        <br/>
                                        <div className="well">
                                            <ul className="list-unstyled project-files">
                                                <li>
                                                    <a href=""><i
                                                        className="fa fa-file"/> Документ.docx
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href=""><i
                                                        className="fa fa-file-picture-o"/> Снимок.jpg
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            )}
            </div>

        )
    }
}

export default DoctorMedcartData;