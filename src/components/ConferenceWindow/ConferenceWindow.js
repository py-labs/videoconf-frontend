import React, {Component} from 'react'
import DoctorDescriptionPanel from "../DoctorDescriptionPanel/DoctorDescriptionPanel";
import ConferenceWindowsTopPanel from "../ConferenceWindowsTopPanel/ConferenceWindowsTopPanel";
import ChatWindow from "../ChatWindow/ChatWindow";
import VideoWindow from "../../containers/PatientVideoWindow/PatientVideoWindow";

class ConferenceWindow extends Component {
    constructor(props){
        super(props);

        this.state = {
            showError: false,
        }
    }

    render() {
        return (
            <div className="row">
                <div className="col-lg-12">

                    <DoctorDescriptionPanel/>

                    <div className="ibox chat-view">

                        <ConferenceWindowsTopPanel/>

                        <div className="ibox-content">

                            <div className="row">

                                <div className="col-md-6 col-lg-6 ">
                                    <ChatWindow/>
                                </div>

                                <div className="col-md-6 col-lg-6 ">
                                    <VideoWindow/>
                                </div>

                            </div>
                        </div>

                    </div>

                    <div className="alert alert-danger alert-dismissable" id="easyrtcErrorDialog" style={{display: this.state.showError ? 'block' : 'none' }}>
                        <button aria-hidden="true" data-dismiss="alert" className="close easyrtcErrorDialog_okayButton" type="button" onClick={ ()=> this.setState({ showError: !this.state.showError })}>×</button>
                        <div id="easyrtcErrorDialog_body">
                            <div className="easyrtcErrorDialog_element">
                            </div>
                        </div>
                    </div>


                </div>

            </div>
        )
    }
}


export default ConferenceWindow;