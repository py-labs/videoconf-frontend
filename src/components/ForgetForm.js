import React, {Component} from 'react'
import {Button, Form} from 'reactstrap';

import TextInput from './TextInput'

export default class ForgetForm extends Component {

    onSubmit = (event) => {
        event.preventDefault()
    }

    componentDidMount() {
        document.body.classList.add('gray-bg')
    }

    componentWillUnmount() {
        document.body.classList.remove('gray-bg')
    }

    render() {
        const errors = this.props.errors || {}

        return (
            <div className="loginColumns animated fadeInDown">
                <div>
                    <div>
                        <h1 className="logo-name">TELEMED</h1>
                    </div>
                    <div className="row">

                        <div className="col-md-6">
                            <h2 className="font-bold">Восстановление пароля</h2>
                            <p>
                                Забыли пароль? Введите имя пользователя или емайл и на вашу почту, указанную при регистрации придет инструкция по его восстановлению.
                            </p>
                        </div>
                        <div className="col-md-6">
                            <div className="ibox-content">
                                <Form className="m-t" role="form">
                                    <TextInput name="username" label="Имя пользователя"
                                               error={errors.username}
                                               onChange={this.handleInputChange}/>
                                    <Button type="submit" className="btn btn-primary block full-width m-b">Отправить</Button>

                                    <hr />
                                    <a className="btn btn-sm btn-white btn-block" href="/login/">Войти</a>
                                </Form>

                            </div>
                        </div>
                    </div>
                    <hr />
                    <div className="row">
                        <div className="col-md-6 text-left">
                            Telemed
                        </div>
                        <div className="col-md-6 text-right">
                            <small>© 2018</small>
                        </div>
                    </div>
                </div>
            </div>


        )
    }
}