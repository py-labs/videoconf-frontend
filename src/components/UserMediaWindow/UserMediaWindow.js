import React, {Component} from 'react'
import {Button} from "reactstrap";

class VideoCallButtons extends Component {

    makeCall() {
        window.easyrtc.hangupAll();
        let successCB = function () {
        };
        let failureCB = function () {
        };
        window.easyrtc.call(this.props.easyrtcid, successCB, failureCB);
    }
    endCall(){
        window.easyrtc.hangupAll();
    }

    render() {
        return (
            <div>
                <Button size='sm' onClick={this.makeCall.bind(this)}>call</Button>&nbsp;&nbsp;
                <Button size='sm' onClick={this.endCall.bind(this)}>end</Button>
            </div>
        )
    }
}


export default VideoCallButtons;