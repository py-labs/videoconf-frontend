import React, {Component} from 'react';
import './SideBar.css'
// import './MetisMenu.css'
import {Link} from "react-router-dom";
import * as reducers from '../../store/reducers'
import {connect} from "react-redux";
import {logout} from "../../store/auth/actions";
import TypeUser from "../../components/TypeUser/TypeUser"

class SideBar extends Component {
    constructor () {
        super()
        this.state = {
            menuShow: false
        };
    }

    logout(e) {
        e.preventDefault();
        this.props.dispatch(logout())
    }

    toggleMenu = (e) => {
        e.preventDefault();
        this.setState({menuShow: !this.state.menuShow})
    }

    activeRoute(routeName) {
        // return this.props.location.pathname.indexOf(routeName) > -1 ? "active" : "";
    }

    render() {
        const {url, username, isDoctor} = this.props;

        let urlConference = isDoctor ? '/conf': '/appointments';


        return (
            <nav className="navbar-default navbar-static-side" role="navigation">
                <ul className="nav metismenu" id="side-menu" ref="menu">
                    <li className="nav-header">
                        <div className="dropdown profile-element">
                            <span>
                            </span>
                            <img alt="image" className="rounded-circle" src="img/usrdefault1.png" />
                            <a data-toggle="dropdown" className="dropdown-toggle" href="#" onClick={this.toggleMenu}>
                                <span className="clear"> <span className="block m-t-xs"> <strong
                                    className="font-bold">{username}</strong>
                                 </span>
                                <TypeUser/>
                                </span>
                            </a>
                            {!isDoctor &&
                            <ul className={"dropdown-menu animated fadeInRight m-t-xs " + (this.state.menuShow ? 'show' : null)} onClick={this.toggleMenu}>
                                <li>
                                    <Link to="/options">
                                        <span>Настройки</span>
                                    </Link>
                                </li>
                                <li className="dropdown-divider"></li>
                                <li><a href="#" onClick={this.logout.bind(this)}> Выйти</a></li>
                            </ul>
                            }
                        </div>
                        <div className="logo-element">
                            IN+
                        </div>
                    </li>
                    {isDoctor &&
                    <li className={this.activeRoute("/appoint")}>
                        <Link to="/appoint"><i className="fa fa-th-large"></i> <span
                            className="nav-label">Ведение приема</span></Link>
                    </li>
                    }
{/*
                        <li className={this.activeRoute("/profile")}>
                            <Link to="/profile"><i className="fa fa-th-large"></i> <span
                                className="nav-label">Профиль</span></Link>
                        </li>
                        <li className={this.activeRoute("/medcart")}>
                            <Link to="/medcart"><i className="fa fa-th-large"></i> <span
                                className="nav-label">Медкарта</span></Link>
                        </li>
                        <li className={this.activeRoute("/sign")}>
                            <Link to="/sign"><i className="fa fa-th-large"></i> <span
                                className="nav-label">Запись онлайн</span></Link>
                        </li>
                        <li className={this.activeRoute("/monitoring")}>
                            <Link to="/monitoring"><i className="fa fa-th-large"></i> <span
                                className="nav-label">Мониторинг</span></Link>
                        </li>
                        <li className={this.activeRoute("/events")}>
                            <Link to="/events"><i className="fa fa-th-large"></i> <span className="nav-label">События</span></Link>
                        </li>
                        )
*/}
                    {!isDoctor &&
                    <li className={this.activeRoute("/medcart")}>
                        <Link to="/medcart"><i className="fa fa-th-large"></i> <span
                            className="nav-label">Медкарта</span></Link>
                    </li>
                    }
                    {!isDoctor &&
                    <li className={this.activeRoute("/record")}>
                        <Link to="/record"><i className="fa fa-th-large"></i> <span
                            className="nav-label">Запись онлайн</span></Link>
                    </li>
                    }
                    {!isDoctor &&
                    <li className={this.activeRoute("/monitoring")}>
                        <Link to="/monitoring"><i className="fa fa-th-large"></i> <span
                            className="nav-label">Мониторинг</span></Link>
                    </li>
                    }
                    {!isDoctor &&
                    <li className={this.activeRoute("/events")}>
                        <Link to="/events"><i className="fa fa-th-large"></i> <span className="nav-label">События</span></Link>
                    </li>
                    }
                    {!isDoctor &&
                    <li className={this.activeRoute({urlConference})}>
                        <Link to={urlConference}><i className="fa fa-th-large"></i> <span
                            className="nav-label">Телеприемная</span></Link>
                    </li>
                    }
                    {!isDoctor &&
                    <li className={this.activeRoute("/files")}>
                        <Link to="/files"><i className="fa fa-th-large"></i> <span
                            className="nav-label">Мои документы</span></Link>
                    </li>
                    }
                </ul>

            </nav>
            /*
            <Navbar className="sidebar navbar-default">
                <NavItem className="sidebar-header">
                    <h3>User Profile</h3>
                    <p>{this.props.username}</p>
                    <a onClick={this.logout.bind(this)}>Выход</a>
                </NavItem>

                <Nav className="list-unstyled components metismenu" navbar>
                    <Link to='/'>
                        <NavItem>
                            Кабинет
                        </NavItem>
                    </Link>
                    <Link to='/conf'>
                        <NavItem>
                            Конференция
                        </NavItem>
                    </Link>
                </Nav>
            </Navbar> */

        )


    }
}


const mapStateToProps = (state) => ({
    username: reducers.getUserFullName(state),
    isDoctor: reducers.isUserDoctor(state)

});

export default connect(mapStateToProps, null)(SideBar);