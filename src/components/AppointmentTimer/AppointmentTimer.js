import React, {Component} from 'react';
import {connect} from 'react-redux';
import {selectedAppointmentDate} from "../../store/reducers";
import Countdown from 'react-countdown-now';

class AppointmentTimer extends Component {
    constructor(props) {
        super(props);

        this.tMessage = 'До начала приема:';
    }

    renderer = ({days, hours, minutes, seconds, completed}) => {
        const Completionist = () => <span>Время ожидания истекло.</span>;
        if (completed) {
            // Render a completed state
            return <Completionist/>;
        } else {
            // Render a countdown
            let messageDaysHours = '';

            days = parseInt(days);
            hours = parseInt(hours);

            if (days)
                messageDaysHours += days + ' д ';
            if (hours)
                messageDaysHours += hours + ' ч';

            return <span>{this.tMessage} {messageDaysHours} {minutes} мин</span>
        }
    };


    render() {
        let {dateAppointment} = this.props;
        dateAppointment = new Date(dateAppointment);

        return (
            <div className="float-left">
                <Countdown
                    date={dateAppointment}
                    renderer={this.renderer}
                />
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    dateAppointment: selectedAppointmentDate(state),
})

export default connect(mapStateToProps, null)(AppointmentTimer);
