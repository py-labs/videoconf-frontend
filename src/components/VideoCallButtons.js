import React, {Component} from 'react'
import {Button} from "reactstrap";
import {Camera} from "../helpers/Helpers";


class VideoCallButtons extends Component {
    constructor(props){
        super(props);
        this.state = {
            isCalling: false
        }
    }

    toggleButtons = () => {
        this.setState({
            isCalling: !this.state.isCalling
        })
    };

    successCall = () => {
        // console.log(window.easyrtc.getRemoteStream(window.easyrtc.getIthCaller(0)));
        let userId = window.easyrtc.getIthCaller(0);
        if (userId){
            const {camera} = this.props;
            camera.init();
            camera.startRecording();
            // console.log(window.easyrtc.getRemoteStream(window.easyrtc.getIthCaller(0)));
        }
    };
    failureCall = () => {
        //none
    };

    makeCall = () => {
        this.toggleButtons();
        window.easyrtc.hangupAll();
        window.easyrtc.call(this.props.easyrtcid, this.successCall, this.failureCall);
    };

    endCall = () => {
        this.toggleButtons();
        this.props.camera.stopRecording();
        window.easyrtc.hangupAll();
        this.props.saveVideo();
    };

    render() {
        const {isCalling} = this.state;

        return (
            <div>
                <Button size='sm' onClick={this.makeCall} className="btn-primary" disabled={isCalling}>
                    Вызов
                </Button>
                &nbsp;&nbsp;
                <Button size='sm' onClick={this.endCall} className="btn-danger" disabled={!isCalling}>
                    Завершить
                </Button>
            </div>
        )
    }
}


export default VideoCallButtons;