import React, {Component} from 'react'
import {Link} from "react-router-dom";
import DateRangePicker from 'react-bootstrap-daterangepicker';
import moment from 'moment';
import "moment/locale/ru"
import 'react-dates/lib/css/_datepicker.css';
import 'bootstrap-daterangepicker/daterangepicker.css';
import Select from 'react-select';

moment.locale('ru');
moment().format('L');

const options = [
    { value: 'приемы', label: 'приемы' },
    { value: 'анализы', label: 'анализы' },
    { value: 'обследования', label: 'обследования' }
];


class MedcartList extends Component {
    constructor () {
        super()

        this.state = {
            focusedInput: null,
            startDate: '',
            endDate: ''

        };
    }

    setSelectedDates = (e, picker) => {
        this.setState({
            startDate: picker.startDate.format('DD.MM.YYYY'),
            endDate: picker.endDate.format('DD.MM.YYYY')
        });
    };
    clearSelectedDates = (e, picker) => {
        this.setState({
            startDate: '',
            endDate: ''
        })
    };

    render() {
        const locale = {
            applyLabel: 'Выбрать',
            cancelLabel: 'Очистить'
        };
        return (
            <div className="ibox">
                <div className="ibox-title justify-content-md-center"><h5>Медицинская карта</h5></div>
                <div className="ibox-content">
                    <div className="row m-b-sm m-t-sm">
                        <div className="col-md-12">
                            <div className="col-md-4">
                                <div className="input-group">
                                    <input type="text" placeholder="Поиск по медкарте" className="form-control-sm form-control"/>
                                    <span className="input-group-btn">
                                        <button type="button" className="btn btn-sm btn-primary"> Искать
                                        </button>
                                    </span>
                                </div>
                            </div>
                            <div className="col-md-4">
                                <Select className="select2-container" classNamePrefix="react-select"
                                        isMulti
                                        placeholder="Фильтр по категориям"
                                        options={options}
                                />
                            </div>
                            <div className="col-md-4">
                                {/*<DateRangePicker
                                startDate={this.state.startDate} // momentPropTypes.momentObj or null,
                                startDateId="date1" // PropTypes.string.isRequired,
                                endDate={this.state.endDate} // momentPropTypes.momentObj or null,
                                endDateId="date2" // PropTypes.string.isRequired,
                                onDatesChange={({ startDate, endDate }) => this.setState({ startDate, endDate })} // PropTypes.func.isRequired,
                                focusedInput={this.state.focusedInput} // PropTypes.oneOf([START_DATE, END_DATE]) or null,
                                onFocusChange={focusedInput => this.setState({ focusedInput })} // PropTypes.func.isRequired,
                            />*/}
                                <DateRangePicker
                                    onApply={this.setSelectedDates}
                                    locale={locale}
                                    onCancel={this.clearSelectedDates}>
                                    <input name="datefilter" className="form-control" placeholder='Фильтр по датам'
                                           value={this.state.startDate ? this.state.startDate + '-' + this.state.endDate : ''}/>
                                </DateRangePicker>
                            </div>
                    </div>
                    </div>
                    <div className="project-list">
                        <table className="table table-hover">
                            <tbody>
                            <tr>
                                <td className="project-status"><span className="label label-primary">Прием</span></td>
                                <td className="project-title">
                                    <Link to="/medcart/detail">
                                        Прием у гастроэнтеролога
                                        <br />
                                        <small>Дата 14.08.2018</small>
                                    </Link>
                                </td>
                                <td className="project-completion">
                                    <small>Текстовое описание</small>
                                </td>
                                <td className="project-people">
                                    <small>Дополнительная информация</small>
                                </td>

                                <td className="project-actions">
                                    <Link to="/medcart/detail" className="btn btn-white btn-sm">
                                        <i className="fa fa-folder" /> Просмотр
                                    </Link>
                                </td>
                            </tr>
                            <tr>
                                <td className="project-status"><span className="label label-warning">Иссл</span></td>
                                <td className="project-title">
                                    <Link to="/medcart/analysis">
                                        Исследование № ЦЕ-0000123133
                                        <br />
                                        <small>Дата 24.08.2018</small>
                                    </Link>
                                </td>
                                <td className="project-completion">
                                    <small>Список анализов</small>
                                </td>
                                <td className="project-people">
                                    <small>Договор №ЦЕ-728123123</small>
                                </td>
                                <td className="project-actions">
                                    <Link to="/medcart/analysis" className="btn btn-white btn-sm">
                                        <i className="fa fa-folder" /> Просмотр
                                    </Link>
                                    <Link to="" className="btn btn-white btn-sm" style={{marginLeft: '5px'}}>
                                        <i className="fa fa-envelope" /> PDF
                                    </Link>
                                </td>

                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        )
    }
}

export default MedcartList;