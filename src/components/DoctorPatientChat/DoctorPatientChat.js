import React, {Component} from 'react'
import {Link} from "react-router-dom";
import ChatWindow from "../ChatWindow/ChatWindow";

class DoctorPatientChat extends Component {
    render() {
        return (
            <ChatWindow />
        )
    }
}

export default DoctorPatientChat;