import React, {Component} from 'react'
import {Link} from "react-router-dom";
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'moment/locale/ru';

import 'react-datepicker/dist/react-datepicker.css';
import './ProfileList.css';

class ProfileList extends Component {
    constructor () {
        super()
        this.state = {
            //startDate: moment()
            startDate: '01.01.2018',
            endDate: '01.02.2018'
        };
    }

    handleChange = (date) => {
        this.setState({
            startDate: date
        });
    }

    render() {

        moment.locale('ru');

        return (
            <div className="row">
                <div className="col-lg-12">

                    <Tabs className="tabs-container">
                        <TabList className="nav nav-tabs">
                            <Tab><a className="nav-link" >Заболевания</a></Tab>
                            <Tab><a className="nav-link" >Диагнозы</a></Tab>
                        </TabList>

                        <TabPanel>
                            <div className="panel-body">
                                <form method="get">
                                    <div className="table-responsive">
                                    <table className="table table-stripped">

                                        <thead>
                                        <tr>
                                            <th style={{width: '15%'}}>
                                                Дата начала
                                            </th>
                                            <th style={{width: '15%'}}>
                                                Дата прекращения
                                            </th>
                                            <th>
                                                Диагноз
                                            </th>
                                            <th style={{width: '15%'}}>
                                                Код по МКБ-10
                                            </th>
                                            <th style={{width: '25%'}}>
                                                Врач
                                            </th>
                                            <th style={{width: '6%'}}>

                                            </th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        <tr>
                                            <td>
                                                {this.state.startDate}
                                            </td>
                                            <td>
                                                {this.state.endDate}
                                            </td>
                                            <td>
                                                Пример диагноза
                                            </td>
                                            <td>
                                                Код МКБ-10
                                            </td>
                                            <td>
                                                ФИО врача
                                            </td>
                                            <td>
                                                <button className="btn btn-white"><i className="fa fa-trash" />
                                                </button>
                                                <button className="btn btn-white"><i className="fa fa-pencil" />
                                                </button>
                                            </td>
                                        </tr>
                                        </tbody>

                                    </table>
                                </div>
                                    <button className="btn btn-primary" type="submit">Добавить</button>
                                </form>
                            </div>
                        </TabPanel>

                        <TabPanel>
                            <div className="panel-body">
                                <form method="get">
                                    <div className="table-responsive">
                                    <table className="table table-stripped">

                                        <thead>
                                        <tr>
                                            <th style={{width: '10%'}}>
                                                Дата
                                            </th>
                                            <th>
                                                Заключительный (уточненный) диагноз
                                            </th>
                                            <th style={{width: '15%'}}>
                                                Установлен впервые или повторно
                                            </th>
                                            <th style={{width: '25%'}}>
                                                Врач
                                            </th>
                                            <th style={{width: '6%'}}>

                                            </th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        <tr>
                                            <td>
                                                {this.state.startDate}
                                            </td>
                                            <td>
                                                Пример диагноза
                                            </td>
                                            <td>
                                                Впервые
                                            </td>
                                            <td>
                                                ФИО врача
                                            </td>
                                            <td>
                                                <button className="btn btn-white"><i className="fa fa-trash" />
                                                </button>
                                                <button className="btn btn-white"><i className="fa fa-pencil" />
                                                </button>
                                            </td>
                                        </tr>
                                        </tbody>

                                    </table>
                                </div>
                                    <button className="btn btn-primary" type="submit">Добавить</button>
                                </form>
                            </div>
                        </TabPanel>

                    </Tabs>

                </div>
            </div>
        )
    }
}

export default ProfileList;