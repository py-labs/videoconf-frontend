import React, {Component} from 'react'

class ChatNewMessage extends Component {


    render() {
        return (
            <div className="form-group">
                <form>
                    <div className="col-md-5 col-lg-5">
                        <div className="chat-message-form">

                            <textarea
                                className="form-control message-input"
                                name="message"
                                id="messageArea"
                                placeholder="Введите сообщение"
                            />


                        </div>
                    </div>
                    <div className="col-lg-1">
                        <button type="submit" className={"btn btn-primary btn-lg btn-circle "}>
                            <i className="fa fa-list"/>
                        </button>
                        <div className="fileinput fileinput-new" data-provides="fileinput" style={{paddingLeft: "10px"}}>
                            <span className="btn btn-primary btn-file btn-circle btn-lg">
                                <i className="fa fa-upload" style={{marginTop: "5px"}}/>
                                <input type="file"name="..."/>
                            </span>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}


export default ChatNewMessage;