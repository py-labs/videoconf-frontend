import React, {Component} from 'react'
import {connect} from "react-redux";
import * as reducers from '../../store/reducers'
import {lastFinishedAppointment} from '../../store/reducers'
import {UPDATE_PATIENT_LIST} from "../../store/doctor/actions";

import {USER_NOT_READY_FOR_VIDEO_CALL} from "../../store/user/actions";

import './DoctorVideoWindow.css'
import Draggable from "react-draggable";

import {isEmpty} from "../../helpers/Helpers";

const SERVER_EASYRTC_ADDRESS = process.env.REACT_APP_SERVER_EASYRTC_ADDRESS || "localhost:8080";


class DoctorVideoWindow extends Component {
    constructor(props) {
        super(props);
    }

    loginSuccess = (easyrtcid) => {
        // let selfEasyrtcid = easyrtcid;
        // this.setEasyId(easyrtcid);
        // document.getElementsByClassName("iam")[0].innerHTML = "I am " + window.easyrtc.cleanId(easyrtcid);
        // document.getElementsByClassName("iam")[0].innerHTML = "I am " + window.easyrtc.cleanId(this.state.username);
    }

    loginFailure = (errorCode, message) => {
        // window.easyrtc.showError(errorCode, message);
    }

    updateUserList = (connectedPatients) => {
        this.props.dispatch({
            type: UPDATE_PATIENT_LIST,
            payload: connectedPatients
        })
    };

    closeVideoStream() {
        window.easyrtc.disconnect();
        window.easyrtc.clearMediaStream(document.getElementById('selfDoctorVideo'));
        window.easyrtc.closeLocalMediaStream();
    }


    saveConnectedPatients = (patients) => {
        this.updateUserList(patients);
    };

    setConnectedPatients = (param1, patientsData, selfData) => {
        let connectedPatients = {};
        if (!isEmpty(patientsData)) {
            for (let key in patientsData) {
                if (patientsData.hasOwnProperty(key)) {
                    let innerDict = patientsData[key];
                    if (innerDict.hasOwnProperty('username') && innerDict.hasOwnProperty('easyrtcid')) {
                        connectedPatients[innerDict['username']] = innerDict['easyrtcid']
                    }
                }
            }
        }
        this.saveConnectedPatients(connectedPatients);
    };

    componentDidUpdate(prevProps) {
        const {isReadyForVideoCall} = this.props;

        if (isReadyForVideoCall !== prevProps.isReadyForVideoCall) {
            if (isReadyForVideoCall) {
                document.getElementById('selfDoctorVideo').style.display = 'block';
                document.getElementById('callerVideo1').style.display = 'block';
                window.easyrtc.hangupAll();
                window.easyrtc.setVideoDims(640, 480);
                window.easyrtc.setUsername(this.props.username);
                window.easyrtc.setSocketUrl(SERVER_EASYRTC_ADDRESS);
                window.easyrtc.setRoomOccupantListener(this.setConnectedPatients);
                window.easyrtc.easyApp("easyrtc.audioVideoSimple", "selfDoctorVideo", ["callerVideo1"], this.loginSuccess, this.loginFailure);
            }
            else {
                document.getElementById('selfDoctorVideo').style.display = 'none';
                this.closeVideoStream();
            }
        }
    }

    componentWillUnmount() {
        this.closeVideoStream();

        this.props.dispatch({
            type: USER_NOT_READY_FOR_VIDEO_CALL
        })
    }

    render() {
        return (
            <Draggable>
                <div className="box patient-window">
                    <figure id='video-main1'>
                        <video autoPlay="autoplay" id="callerVideo1"/>
                    </figure>
                </div>
            </Draggable>
        )
    }
}


const mapStateToProps = (state) => ({
    username: reducers.userLogin(state),
    finishedAppointment: lastFinishedAppointment(state),
    isReadyForVideoCall: reducers.isUserReadyForVideoCall(state)
});

export default connect(mapStateToProps, null)(DoctorVideoWindow);