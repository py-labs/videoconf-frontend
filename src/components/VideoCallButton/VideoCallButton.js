import React, {Component} from 'react'
import {USER_READY_FOR_VIDEO_CALL, USER_NOT_READY_FOR_VIDEO_CALL} from "../../store/user/actions";
import {connect} from "react-redux";
import {isUserDoctor, isUserReadyForVideoCall} from "../../store/reducers";
import {saveAppointmentEnd, saveAppointmentStart} from "../../store/doctor/actions";

const READY_FOR_APPOINTMENT = 'Готов';
const NOT_READY_FOR_APPOINTMENT = 'Отключить';

class VideoCallButton extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            buttonText: props.buttonName || READY_FOR_APPOINTMENT,
            enableButton: false
        };
    }

    changeButtonText(text) {
        this.setState({
            buttonText: text,
            enableButton: !this.state.enableButton
        })
    }

    setReadyFoVideoCall() {
        const {isUserReadyForVideoCall, buttonName, disabledButtonName} = this.props;
        const enabledButtonName = buttonName || READY_FOR_APPOINTMENT;
        const disabledButtonNameCurrent = disabledButtonName || NOT_READY_FOR_APPOINTMENT;
        let typeAction = isUserReadyForVideoCall ? USER_NOT_READY_FOR_VIDEO_CALL : USER_READY_FOR_VIDEO_CALL;
        let buttonText = isUserReadyForVideoCall ? enabledButtonName : disabledButtonNameCurrent;

        // this.saveDateAppointment();

        this.changeButtonText(buttonText);
        this.props.dispatch({
            type: typeAction
        })
    }
    saveDateAppointment(){
        //save start or end date
        const {enableButton} = this.state;
        const {appointment, isDoctor} = this.props;
        if (isDoctor && appointment > 0)
            if (!enableButton)
                this.props.dispatch(saveAppointmentStart(appointment));
            else{
                this.props.dispatch(saveAppointmentEnd(appointment));
                this.props.finishAppointment();
            }
    }
    renderHint(appointment) {
        if (appointment < 0)
            return (
                <p className="text-center">Выберите пациента</p>
            )
    }

    render() {
        const {appointment, isDoctor} = this.props;
        return (
            <div>
                <div className={this.props.isRight ? "float-right ibox-button": "text-center ibox-button"}>
                    <button
                        className="btn btn-primary "
                        type="button"
                        disabled={!isDoctor || appointment > -1 ? '': 'disabled'}
                        style={{marginLeft: "10px"}}
                        onClick={this.setReadyFoVideoCall.bind(this)}
                    >
                        {this.state.buttonText}
                    </button>
                    {this.renderHint(appointment)}
                </div>
            </div>
        )
    }
}
const mapStateToProps = (state) => ({
    isDoctor: isUserDoctor(state),
    isUserReadyForVideoCall: isUserReadyForVideoCall(state),
})

export default connect(mapStateToProps, null)(VideoCallButton);