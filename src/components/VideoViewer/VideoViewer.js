import React, {Component} from 'react'
import {Player} from "video-react";
import './video-react.css';

class VideoViewer extends Component {
    getVideo() {
        const {video} = this.props;
        if (video) {
            return (
                <Player>
                    <source src={video}/>
                </Player>
            )
        }
        else {
            return (
                <div>Видео пока нет</div>
            )
        }
    }
    getAudio() {
        const {audio} = this.props;
        if (audio) {
            return (
                <audio controls>
                    <source src={audio} type="audio/ogg; codecs=vorbis"/>
                    <source src={audio} type="audio/mpeg"/>
                </audio>
            )
        }
        else {
            return (
                <div>Аудио пока нет</div>
            )
        }
    }

    render() {
        return (
            <div className="row">
                <div className="col-lg-12">
                    <div className="panel-body">
                        {this.getVideo()}
                        {this.getAudio()}
                    </div>
                </div>
            </div>
        )
    }
}

export default VideoViewer