import React, {Component} from 'react'
import {Link} from "react-router-dom";
import "./MedcartFiles.css"
import Editable from 'react-x-editable';
import "../footable.css"

class MedcartFiles extends Component {
    constructor () {
        super()
        this.state = {
            detailShow: false
        };
    }

    toggleDetail = (e) => {
        e.preventDefault();
        this.setState({detailShow: !this.state.detailShow})
    }

    render() {
        return(
            <div className="row">
                <div className="col-lg-12">
                    <div className="ibox ">
                        <div className="ibox-title">
                            <h5>Мои документы</h5>
                        </div>
                        <div className="ibox-content">
                            <div className="form-group row">
                                <div className="col-sm-4">
                                    <button className="btn btn-primary" type="submit">Загрузить файл</button>
                                </div>
                            </div>
                            <div className="table-responsive m-t-sm ">
                                <input type="text" className="form-control form-control-sm m-b-xs" id="filter" placeholder="Поиск по файлам" />
                                <table className="footable breakpoint footable-loaded table table-striped">
                                    <thead>
                                        <tr>
                                            <th style={{width: '2%'}} ></th>
                                            <th className="footable-visible footable-sortable" style={{width: '20%'}}>Файл <span className="footable-sort-indicator" /></th>
                                            <th>Комментарий</th>
                                            <th className="footable-visible footable-sortable" style={{width: '15%'}}>Дата загрузки <span className="footable-sort-indicator" /></th>
                                            <th style={{width: '7%'}}></th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <i className="fa fa-file" />
                                            </td>
                                            <td>
                                                <a href="">Документ.docx</a>
                                            </td>
                                            <td>
                                                <Editable
                                                    dataType="text"
                                                    value="Описание документа"
                                                    showButtons={false}
                                                />
                                            </td>
                                            <td>
                                                31.01.2017
                                            </td>
                                            <td>
                                                <button className="btn btn-white"><i className="fa fa-download" /></button>
                                                <button className="btn btn-white"><i className="fa fa-trash" /></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i className="fa fa-file-picture-o" />
                                            </td>
                                            <td>
                                                <a href="">Снимок.jpg</a>
                                            </td>
                                            <td>
                                                <Editable
                                                    dataType="text"
                                                    value="Описание документа"
                                                    showButtons={false}
                                                />
                                            </td>
                                            <td>
                                                01.02.2017
                                            </td>
                                            <td>
                                                <button className="btn btn-white"><i className="fa fa-download" /></button>
                                                <button className="btn btn-white"><i className="fa fa-trash" /></button>
                                            </td>
                                        </tr>

                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colSpan="5" className="footable-visible">
                                                <ul className="pagination float-right">
                                                    <li className="footable-page-arrow disabled"><a data-page="first" href="">«</a></li>
                                                    <li className="footable-page-arrow disabled"><a data-page="prev" href="">‹</a></li>
                                                    <li className="footable-page active"><a data-page="0" href="">1</a></li>
                                                    <li className="footable-page"><a data-page="1" href="">2</a></li>
                                                    <li className="footable-page"><a data-page="2" href="">3</a></li>
                                                    <li className="footable-page"><a data-page="3" href="">4</a></li>
                                                    <li className="footable-page"><a data-page="4" href="">5</a></li>
                                                    <li className="footable-page-arrow"><a data-page="next" href="">›</a></li>
                                                    <li className="footable-page-arrow"><a data-page="last" href="">»</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                            {/*<div className="form-group row">
                                <div className="col-sm-4">
                                    <button className="btn btn-primary" type="submit">Запросить PDF</button>
                                </div>
                            </div>*/}
                        </div>

                    </div>
                </div>
            </div>

        )
    }
}

export default MedcartFiles;