import React, {Component} from 'react'

// import './.css';

class DoctorPatientData extends Component {
    constructor () {
        super()
        this.state = {
            //startDate: moment()
            startDate: '01.01.2018'
        };
    }

    render() {

        return(
            <div className="row">
                <div className="col-lg-12">
                    <form method="get">
                        <div className="panel-body">
                            <fieldset>
                                <div className="form-group row">
                                    <div className="col-lg-6">
                                        <dl className="row mb-0">
                                            <div className="col-sm-4 text-sm-right">
                                                <dt>Фамилия, имя, отчество:</dt>
                                            </div>
                                            <div className="col-sm-8 text-sm-left">
                                                <dd className="mb-1">Иванов Иван Иванович</dd>
                                            </div>
                                        </dl>
                                        <dl className="row mb-0">
                                            <div className="col-sm-4 text-sm-right">
                                                <dt>Адрес:</dt>
                                            </div>
                                            <div className="col-sm-8 text-sm-left">
                                                <dd className="mb-1">Вологодская область, Герцена, 79, 146</dd>
                                            </div>
                                        </dl>
                                        <dl className="row mb-0">
                                            <div className="col-sm-4 text-sm-right">
                                                <dt>Полис ОМС:</dt>
                                            </div>
                                            <div className="col-sm-8 text-sm-left">
                                                <dd className="mb-1"> АЕ123343</dd>
                                            </div>
                                        </dl>
                                        <dl className="row mb-0">
                                            <div className="col-sm-4 text-sm-right">
                                                <dt>Документ:</dt>
                                            </div>
                                            <div className="col-sm-8 text-sm-left">
                                                <dd className="mb-1">Паспорт</dd>
                                            </div>
                                        </dl>
                                        <dl className="row mb-0">
                                            <div className="col-sm-4 text-sm-right">
                                                <dt>Полис ОМС:</dt>
                                            </div>
                                            <div className="col-sm-8 text-sm-left">
                                                <dd className="mb-1">97192934</dd>
                                            </div>
                                        </dl>
                                        <dl className="row mb-0">
                                            <div className="col-sm-4 text-sm-right">
                                                <dt>Полис ДМС:</dt>
                                            </div>
                                            <div className="col-sm-8 text-sm-left">
                                                <dd className="mb-1">971929340001212</dd>
                                            </div>
                                        </dl>

                                    </div>
                                    <div className="col-lg-6">
                                        <dl className="row mb-0">
                                            <div className="col-sm-4 text-sm-right">
                                                <dt>Дата рождения:</dt>
                                            </div>
                                            <div className="col-sm-8 text-sm-left">
                                                <dd className="mb-1">26 января 1970</dd>
                                            </div>
                                        </dl>
                                        <dl className="row mb-0">
                                            <div className="col-sm-4 text-sm-right">
                                                <dt>Пол:</dt>
                                            </div>
                                            <div className="col-sm-8 text-sm-left">
                                                <dd className="mb-1">М</dd>
                                            </div>
                                        </dl>
                                        <dl className="row mb-0">
                                            <div className="col-sm-4 text-sm-right">
                                                <dt>Местность:</dt>
                                            </div>
                                            <div className="col-sm-8 text-sm-left">
                                                <dd className="mb-1">Городская</dd>
                                            </div>
                                        </dl>
                                        <dl className="row mb-0">
                                            <div className="col-sm-4 text-sm-right">
                                                <dt>Реквизиты документа:</dt>
                                            </div>
                                            <div className="col-sm-8 text-sm-left">
                                                <dd className="mb-1">1000 0101010 выдан УВД г.Вологды</dd>
                                            </div>
                                        </dl>
                                        <dl className="row mb-0">
                                            <div className="col-sm-4 text-sm-right">
                                                <dt>СНИЛС:</dt>
                                            </div>
                                            <div className="col-sm-8 text-sm-left">
                                                <dd className="mb-1">СНИЛС:</dd>
                                            </div>
                                        </dl>
                                        <dl className="row mb-0">
                                            <div className="col-sm-4 text-sm-right">
                                                <dt>Страховая компания:</dt>
                                            </div>
                                            <div className="col-sm-8 text-sm-left">
                                                <dd className="mb-1">Альфа-Страхование</dd>
                                            </div>
                                        </dl>
                                    </div>
                                </div>
                                <div className="hr-line-dashed"></div>
                                <div className="form-group row">
                                    <div className="col-lg-6">
                                        <dl className="row mb-0">
                                            <div className="col-sm-4 text-sm-right">
                                                <dt>Семейное положение:</dt>
                                            </div>
                                            <div className="col-sm-8 text-sm-left">
                                                <dd className="mb-1">не состоит в браке</dd>
                                            </div>
                                        </dl>
                                        <dl className="row mb-0">
                                            <div className="col-sm-4 text-sm-right">
                                                <dt>Занятость:</dt>
                                            </div>
                                            <div className="col-sm-8 text-sm-left">
                                                <dd className="mb-1">проходит военную службу</dd>
                                            </div>
                                        </dl>
                                        <dl className="row mb-0">
                                            <div className="col-sm-4 text-sm-right">
                                                <dt>Место работы, должность:</dt>
                                            </div>
                                            <div className="col-sm-8 text-sm-left">
                                                <dd className="mb-1">ГПЗ, слесарь</dd>
                                            </div>
                                        </dl>
                                    </div>
                                    <div className="col-lg-6">
                                        <dl className="row mb-0">
                                            <div className="col-sm-4 text-sm-right">
                                                <dt>Образование:</dt>
                                            </div>
                                            <div className="col-sm-8 text-sm-left">
                                                <dd className="mb-1">среднее специальное</dd>
                                            </div>
                                        </dl>
                                        <dl className="row mb-0">
                                            <div className="col-sm-4 text-sm-right">
                                                <dt>Инвалидность:</dt>
                                            </div>
                                            <div className="col-sm-8 text-sm-left">
                                                <dd className="mb-1">первичная, I, 10.01.2000у</dd>
                                            </div>
                                        </dl>
                                        <dl className="row mb-0">
                                            <div className="col-sm-4 text-sm-right">
                                                <dt></dt>
                                            </div>
                                            <div className="col-sm-8 text-sm-left">
                                                <dd className="mb-1"></dd>
                                            </div>
                                        </dl>
                                    </div>
                                </div>

                                <div className="hr-line-dashed"></div>
                                <div className="form-group row">
                                    <div className="col-lg-6">
                                        <dl className="row mb-0">
                                            <div className="col-sm-4 text-sm-right">
                                                <dt>Группа крови:</dt>
                                            </div>
                                            <div className="col-sm-8 text-sm-left">
                                                <dd className="mb-1">I</dd>
                                            </div>
                                        </dl>
                                        <dl className="row mb-0">
                                            <div className="col-sm-4 text-sm-right">
                                                <dt>Аллергические реакции:</dt>
                                            </div>
                                            <div className="col-sm-8 text-sm-left">
                                                <dd className="mb-1">непереносимость</dd>
                                            </div>
                                        </dl>
                                    </div>
                                    <div className="col-lg-6">
                                        <dl className="row mb-0">
                                            <div className="col-sm-4 text-sm-right">
                                                <dt>Rh-фактор:</dt>
                                            </div>
                                            <div className="col-sm-8 text-sm-left">
                                                <dd className="mb-1">-</dd>
                                            </div>
                                        </dl>
                                        <dl className="row mb-0">
                                            <div className="col-sm-4 text-sm-right">
                                                <dt></dt>
                                            </div>
                                            <div className="col-sm-8 text-sm-left">
                                                <dd className="mb-1"></dd>
                                            </div>
                                        </dl>
                                    </div>
                                </div>

                                <div className="hr-line-dashed"></div>
                                <div className="form-group row">
                                    <label className="col-lg-12 col-form-label">Заболевания, по поводу которых осуществляется диспансерное наблюдение:</label>
                                </div>
                                <div className="table-responsive">
                                    <table className="table table-stripped">
                                        <thead>
                                        <tr>
                                            <th style={{width: '15%'}}>
                                                Дата начала
                                            </th>
                                            <th style={{width: '15%'}}>
                                                Дата прекращения
                                            </th>
                                            <th>
                                                Диагноз
                                            </th>
                                            <th style={{width: '15%'}}>
                                                Код по МКБ-10
                                            </th>
                                            <th style={{width: '25%'}}>
                                                Врач
                                            </th>
                                            <th style={{width: '11%'}}>

                                            </th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        <tr>
                                            <td>
                                                {this.state.startDate}
                                            </td>
                                            <td>
                                                {this.state.endDate}
                                            </td>
                                            <td>
                                                Пример диагноза
                                            </td>
                                            <td>
                                                Код МКБ-10
                                            </td>
                                            <td>
                                                ФИО врача
                                            </td>
                                            <td>
                                                <button className="btn btn-white"><i className="fa fa-trash" />
                                                </button>
                                                <button className="btn btn-white"><i className="fa fa-pencil" />
                                                </button>
                                            </td>
                                        </tr>
                                        </tbody>

                                    </table>
                                    <button className="btn btn-primary" type="submit">Добавить</button>
                                </div>
                            </fieldset>
                            {/*<div className="form-group row">
                                <div className="col-lg-10 col-lg-offset-0">
                                    <button className="btn btn-white btn-sm" type="submit">Отмена</button>
                                    <button className="btn btn-primary" type="submit">Сохранить изменения</button>
                                </div>
                            </div>*/}
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}

export default DoctorPatientData;