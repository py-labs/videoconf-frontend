import React, {Component} from 'react';
import './CitySelect.css'

class CitySelect extends React.Component {
    constructor () {
        super()
        this.state = {
            menuShow: false
        };
    }

    toggleMenu = (e) => {
        e.preventDefault();
        this.setState({menuShow: !this.state.menuShow})
        console.log(this.state.menuShow)
    }

    render() {
        return (
            <div>
                <span>Ваш город:</span>
                <a className="dropdown-toggle count-info" data-toggle="dropdown" href="#" aria-expanded="false" onClick={this.toggleMenu}>
                    <strong>Вологда</strong>
                </a>
                <ul className={"dropdown-menu dropdown-alerts " + (this.state.menuShow ? 'show' : null)} onClick={this.toggleMenu}>
                    <li>
                        <a href="" className="dropdown-item" >
                            <div>
                                Вологда
                            </div>
                        </a>
                    </li>
                    <li className="dropdown-divider"></li>
                    <li>
                        <a href="" className="dropdown-item">
                            <div>
                                Череповец
                            </div>
                        </a>
                    </li>
                    <li className="dropdown-divider"></li>
                    <li>
                        <a href="" className="dropdown-item">
                            <div>
                                Вельск
                            </div>
                        </a>
                    </li>

                </ul>
            </div>
        );
    }
}

export default CitySelect