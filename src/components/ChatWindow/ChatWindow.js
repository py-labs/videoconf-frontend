import React, {Component} from 'react'
import {database } from '../../firebase/firebase';
import {serverUserInfo} from "../../store/reducers";
import connect from "react-redux/es/connect/connect";
//import "./ChatWindow.css"

class ChatWindow extends Component {
    constructor(){
        super()
        this.state = {
            message: '',
            messages: [],
            user: null
        }
    }

    componentWillMount() {
        const messagesRef = database.ref('messages')
            .orderByKey()
            .limitToLast(100);

        messagesRef.on('child_added', snapshot => {
            const m = snapshot.val();
            const message = {
                senderId: m.senderId,
                datetime: m.datetime,
                text: m.text
            };
            // console.log('firebase', m);
            this.setState(prevState => ({
                messages: [ message, ...prevState.messages ],
            }));
        });
    }

    handleChange = (e) => {
        this.setState({
            message: e.target.value
        })
        let h = e.target.scrollHeight+"px"
        document.getElementById("messageArea").style.height = h
    }

    handleSubmit = (e) => {
        e.preventDefault()
        if (this.state.message !== '') {
            this.sendMessage(this.state.message)
            this.setState({
                message: ''
            })
            document.getElementById("messageArea").style.height = "50px"
        }
    }

    onEnterPress = (e) => {
        if(e.keyCode === 13 && e.ctrlKey === false) {
            e.preventDefault();
            this.handleSubmit(e);
        }
    }

    sendMessage = (mes) => {
        // event.preventDefault();
        let data = {
            senderId: this.props.user.username,
            datetime: Date.now(),
            text: mes
        }
        database.ref('messages').push(data);
    }

    getDate (d) {
        if (d !== ''){
            let date = new Date(d);
            return (date.toLocaleDateString() + ' ' + date.getHours() + ':' + date.getMinutes() );
        }
        else{
            return '';
        }
    }

    scrollToBottom = () => {
        this.messagesEnd.scrollIntoView({ behavior: "smooth" });
    }

    componentDidUpdate() {
        this.scrollToBottom();
    }

    componentDidMount() {
        //this.scrollToBottom();
        //console.log('didmount', this.props.user.username)
        this.setState({user: this.props.user.username})
    }

    render() {
        return (
            <div className="ibox ">
                <div className="ibox-content">
                    <div className="chat-discussion">
                        {console.log('render', this.state.user)}
                        {[...this.state.messages].reverse().map((message, key) => {
                            return (
                                <div key={key}>
                                    {(message.senderId) === this.state.user &&
                                    <div className="chat-message right">
                                        <img className="message-avatar" src="img/usrdefault1.png" alt="" />
                                        <div className="message">
                                            {/*<a className="message-author" href="#"> {message.senderId} </a>*/}
                                            <span className="message-date">
                                                                            {this.getDate(message.datetime)}
                                                                        </span>
                                            <span className="message-content">
                                                                            {message.text}
                                                                        </span>
                                        </div>
                                    </div>
                                    }

                                    {(message.senderId !== this.state.user) &&
                                    <div className="chat-message left" key={key}>
                                        <img className="message-avatar" src="img/usrdefault2.png" alt="" />
                                        <div className="message">
                                            {/*<a className="message-author" href="#"> {message.senderId} </a>*/}
                                            <span className="message-date">
                                                                                {this.getDate(message.datetime)}
                                                                            </span>
                                            <span className="message-content">
                                                                                {message.text}
                                                                            </span>
                                        </div>
                                    </div>
                                    }
                                </div>
                            )
                        })}
                        <div style={{ float:"left", clear: "both" }}
                             ref={(el) => { this.messagesEnd = el; }}>
                        </div>
                    </div>

                    <div className="row">
                        <div className="form-group">
                            <form onSubmit={this.handleSubmit} ref={el => this.myFormRef = el}>
                                <div className="col-md-11 col-lg-11">
                                    <div className="chat-message-form">
                                        <textarea
                                            onChange={this.handleChange}
                                            value={this.state.message}
                                            onKeyDown={this.onEnterPress}
                                            className="form-control message-input"
                                            name="message"
                                            id="messageArea"
                                            placeholder="Введите сообщение"
                                        />
                                    </div>
                                </div>
                                <div className="col-lg-1">
                                    <button type="submit" className={"btn btn-primary btn-lg btn-circle "}>
                                        <i className="fa fa-paper-plane" style={{marginLeft: "-5px"}}></i>
                                    </button>
                                    {/*<div className="fileinput fileinput-new" data-provides="fileinput" style={{paddingLeft: "10px"}}>
                                                <span className="btn btn-primary btn-file btn-circle btn-lg">
                                                    <i className="fa fa-upload" style={{marginTop: "5px"}}></i>
                                                    <input type="file" name="..."/>
                                                </span>
                                            </div>*/}
                                </div>
                            </form>
                        </div>
                    </div>

                </div>

            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    user: serverUserInfo(state)
})

export default connect(mapStateToProps, null)(ChatWindow);
