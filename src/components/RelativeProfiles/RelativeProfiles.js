import React, {Component} from 'react'
import "./RelativeProfiles.css"
import {Link} from "react-router-dom";

class RelativeProfiles extends Component {
    render() {
        return (
            <div className="ibox">
                <div className="ibox-content">
                    <div className="project-list">
                        <table className="table table-hover">

                            <thead>
                            <tr>
                                <th style={{width: '5%'}}>
                                </th>
                                <th>
                                    Аккаунт
                                </th>
                                <th>
                                    Права доступа
                                </th>
                                <th style={{width: '5%'}}>
                                    Удалить
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td className="project-people">
                                    <img alt="image" className="rounded-circle" src="img/usrdefault1.png" />
                                </td>
                                <td className="project-title">
                                    <span>ФИО аккаунта</span>
                                </td>
                                <td className="project-title">
                                    <span>Доступ</span>
                                </td>
                                <td className="project-actions">
                                    <Link to="" className="btn btn-white btn-sm">
                                        <i className="fa fa-trash" />
                                    </Link>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                    {/*<button className="btn btn-white btn-sm" type="submit">Отмена</button>*/}
                    <button className="btn btn-primary" type="submit">Добавить профиль</button>

                </div>
            </div>
        )
    }
}

export default RelativeProfiles;