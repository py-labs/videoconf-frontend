import React, {Component} from 'react'
import {Button, Form} from 'reactstrap';

import TextInput from './TextInput'

export default class LoginForm extends Component {
    state = {
        username: '',
        password: ''
    }

    handleInputChange = (event) => {
        const target = event.target,
            value = target.type === 'checkbox' ? target.checked : target.value,
            name = target.name

        this.setState({
            [name]: value
        })
    }

    onSubmit = (event) => {
        event.preventDefault()
        this.props.onSubmit(this.state.username, this.state.password)
    }

    componentDidMount() {
        document.body.classList.add('gray-bg')
    }

    componentWillUnmount() {
        document.body.classList.remove('gray-bg')
    }

    render() {
        const errors = this.props.errors || {}

        return (
            <div className="loginColumns animated fadeInDown">
                <div>
                    <div>
                        <h1 className="logo-name">TELEMED</h1>
                    </div>
                    <div className="row">

                        <div className="col-md-6">
                            <h2 className="font-bold">Добро пожаловать!</h2>
                            <p>
                                Мы рады приветствовать в системе TELEMED
                            </p>
                            <p>
                                Я принимаю <u>условия и соглашение</u> на использование сервиса.
                            </p>


                        </div>
                        <div className="col-md-6">
                            <div className="ibox-content">
                                <Form className="m-t" role="form" onSubmit={this.onSubmit}>
                                    <TextInput name="username" label="Имя пользователя"
                                               error={errors.username}
                                               onChange={this.handleInputChange}/>
                                    <TextInput name="password" label="Пароль"
                                               error={errors.password} type="password"
                                               onChange={this.handleInputChange}/>
                                    <Button type="submit" className="btn btn-primary block full-width m-b">Войти</Button>

                                    <a href="/forget/">
                                        <small>Забыли пароль?</small>
                                    </a>
                                    <hr />
                                    <p className="text-muted text-center">
                                        Еще не зарегистрированы и у вас не установлено наше приложение?
                                    </p>
                                    <a className="btn btn-sm btn-white btn-block" href="/register/">Зарегистрироваться</a>
                                </Form>

                            </div>
                        </div>
                    </div>
                    <hr />
                    <div className="row">
                        <div className="col-md-6 text-left">
                            Telemed
                        </div>
                        <div className="col-md-6 text-right">
                            <small>© 2018</small>
                        </div>
                    </div>
                </div>


                {/*<Jumbotron className="container">
                    <Form onSubmit={this.onSubmit}>
                        <h1>Authentication</h1>
                        {
                            errors.non_field_errors?
                                <Alert color="danger">
                                    {errors.non_field_errors}
                                </Alert>: ""
                        }
                        <TextInput name="username" label="Username"
                                   error={errors.username}
                                   onChange={this.handleInputChange}/>
                        <TextInput name="password" label="Password"
                                   error={errors.password} type="password"
                                   onChange={this.handleInputChange}/>
                        <Button type="submit" color="primary" size="lg">
                            Log in
                        </Button>
                    </Form>
                </Jumbotron>*/}


            </div>


        )
    }
}