import React, {Component} from 'react'
import {Link} from "react-router-dom";
import "./MedcartAnalysis.css"
import "../footable.css"


class MedcartAnalysis extends Component {
    constructor () {
        super()
        this.state = {
            detailShow: false
        };
    }

    toggleDetail = (e) => {
        e.preventDefault();
        this.setState({detailShow: !this.state.detailShow})
    }

    render() {
        return(
            <div className="row">
                <div className="col-lg-12">
                    <div className="ibox ">
                        <div className="ibox-title">
                            <h5>Результаты исследования</h5>
                        </div>
                        <div className="ibox-content">
                            <div className="row">
                                <div className="col-lg-12">
                                    <div className="m-b-md">
                                        <Link to="/medcart" className="btn btn-primary btn-xs float-right">
                                            К списку
                                        </Link>
                                        <h2>Результаты исследований
                                            № ЦЕ-0000123123 от 10.06.2016 , Договор №ЦЕ-728123123()</h2>
                                    </div>
                                </div>
                            </div>
                            <div className="row m-t-sm">
                                <div className="col-lg-6">
                                    <dl className="row mb-0">
                                        <div className="col-sm-4 text-sm-right">
                                            <dt>Дата взятия пробы:</dt>
                                        </div>
                                        <div className="col-sm-8 text-sm-left">
                                            <dd className="mb-1">10.06.2016 (08:57)</dd>
                                        </div>
                                    </dl>
                                    <dl className="row mb-0">
                                        <div className="col-sm-4 text-sm-right">
                                            <dt>Адрес лаборатории:</dt>
                                        </div>
                                        <div className="col-sm-8 text-sm-left">
                                            <dd className="mb-1">Зосимовская ул, дом №53а</dd>
                                        </div>
                                    </dl>
                                </div>
                                <div className="col-lg-6" id="cluster_info">

                                    <dl className="row mb-0">
                                        <div className="col-sm-4 text-sm-right">
                                            <dt></dt>
                                        </div>
                                        <div className="col-sm-8 text-sm-left">
                                            <dd className="mb-1"></dd>
                                        </div>
                                    </dl>
                                    <dl className="row mb-0">
                                        <div className="col-sm-4 text-sm-right">
                                        </div>
                                        <div className="col-sm-8 text-sm-left">
                                            <dd className="mb-1"></dd>
                                            <dt></dt>
                                        </div>
                                    </dl>
                                </div>
                            </div>
                            <div className="form-group row">
                                <div className="col-sm-4">
                                    <button className="btn btn-primary" type="submit">Запросить PDF</button>
                                </div>
                            </div>
                            <div className="table-responsive m-t-sm ">
                                <table className="footable breakpoint footable-loaded table table-striped">
                                    <thead>
                                    <tr>
                                        <th style={{width: '20%'}}>Исследование</th>
                                        <th>Биол. материал</th>
                                        <th>Метод исследования</th>
                                        <th colspan="2">Результат</th>
                                        <th>Референтные значения</th>
                                        <th>Статус</th>
                                        <th>Дата обновления</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>
                                            Глюкоза в плазме крови
                                        </td>
                                        <td>
                                            кровь
                                        </td>
                                        <td>
                                            биохимический
                                        </td>
                                        <td><i className="fa fa-caret-down" style={{color: "#ed5565"}} /></td>
                                        <td>5,42</td>
                                        <td>18-60 лет 3,9-5,9<br />
                                            60-90 лет 4,6-6,4<br />
                                            старше 90 лет 4,2-6,7</td>
                                        <td>
                                            <span className="label label-primary">Готов</span>
                                        </td>
                                        <td>14:45 31.10.2018</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Холестерин
                                        </td>
                                        <td>
                                            кровь
                                        </td>
                                        <td>
                                            биохимический
                                        </td>
                                        <td><i className="fa fa-caret-up" style={{color: "#ed5565"}} /></td>
                                        <td>5,5</td>
                                        <td>3 - 5,2 </td>
                                        <td>
                                            <span className="label label-warning">Ожидание контроля</span>
                                        </td>
                                        <td>14:45 31.10.2018</td>
                                    </tr>
                                    <tr className={"footable-even" + (this.state.detailShow ? ' footable-detail-show' : null)}>
                                        <td onClick={this.toggleDetail}>
                                            <span className="footable-toggle"></span>
                                            Общий анализ мочи
                                        </td>
                                        <td>
                                            моча
                                        </td>
                                        <td>
                                            биохимический
                                        </td>
                                        <td> </td>
                                        <td> </td>
                                        <td> </td>
                                        <td>
                                            <span className="label label-danger">В работе</span>
                                        </td>
                                        <td>14:55 31.10.2018</td>
                                    </tr>
                                    <tr className="footable-row-detail" style={(!this.state.detailShow ? {display: 'none'} : null)}>
                                        <td>
                                            Цвет
                                        </td>
                                        <td> </td>
                                        <td> </td>
                                        <td><i className="fa fa-plus" style={{color: "#1ab394"}} /></td>
                                        <td>
                                            Желтый
                                        </td>
                                        <td>
                                            Светло-желтый
                                        </td>
                                        <td> </td>
                                        <td> </td>
                                    </tr>
                                    <tr className="footable-row-detail" style={(!this.state.detailShow ? {display: 'none'} : null)}>
                                        <td>
                                            Слизь
                                        </td>
                                        <td> </td>
                                        <td> </td>
                                        <td><i className="fa fa-plus" style={{color: "#1ab394"}} /></td>
                                        <td>
                                            Отсутствует
                                        </td>
                                        <td>
                                            Отсутствует
                                        </td>
                                        <td> </td>
                                        <td> </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            {/*<div className="form-group row">
                                <div className="col-sm-4">
                                    <button className="btn btn-primary" type="submit">Запросить PDF</button>
                                </div>
                            </div>*/}
                        </div>

                    </div>
                </div>
            </div>

        )
    }
}

export default MedcartAnalysis;