import React, {Component} from 'react';
import {connect} from 'react-redux';
import AppointmentTimer from "../AppointmentTimer/AppointmentTimer";
import VideoCallButton from "../VideoCallButton/VideoCallButton";
import {isUserDoctor} from "../../store/reducers";

class ConferenceWindowsTopPanel extends Component {


    render() {
        const {isDoctor} = this.props;

        return (
            <div className="ibox-title">
                <small className="float-right text-muted"/>
                {!isDoctor ? <AppointmentTimer/>: null}
                <VideoCallButton isRight={true} />
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    isDoctor: isUserDoctor(state),
})

export default connect(mapStateToProps)(ConferenceWindowsTopPanel);