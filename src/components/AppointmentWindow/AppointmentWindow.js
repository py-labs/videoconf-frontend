import React, {Component} from 'react'
import {Tab, TabList, TabPanel, Tabs} from 'react-tabs';
import DoctorAppointmentData from "../DoctorAppointmentData/DoctorAppointmentData"
import DoctorMedcartData from "../DoctorMedcartData/DoctorMedcartData"
import DoctorPatientData from "../DoctorPatientData/DoctorPatientData"
import DoctorPatientChat from "../DoctorPatientChat/DoctorPatientChat"
import VideoCallButtons from "../VideoCallButtons";
import {
    isUserReadyForVideoCall, lastFinishedAppointment, serverPatientList,
    serverUserInfo
} from "../../store/reducers";
import connect from "react-redux/es/connect/connect";
import {appointmentList, saveAppointmentVideo} from '../../store/doctor/actions'
import DoctorVideoWindow from "../DoctorVideoWindow/DoctorVideoWindow";
import './AppointmentWindow.css';
import VideoCallButton from "../VideoCallButton/VideoCallButton";
import VideoViewer from "../VideoViewer/VideoViewer";
import {CameraContainer} from "../../helpers/Helpers";


class MedcartList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            tabIndex: 0,
            activeAppointment: -1,
            camera: new CameraContainer()
        };
    }

    componentDidMount() {
        this.props.dispatch(appointmentList())
    }

    componentDidUpdate(prevProps) {
      if (this.props.finishedAppointment !== prevProps.finishedAppointment)
        this.props.dispatch(appointmentList())
    }

    onSelectTab(tabIndex) {
        const {patients} = this.props;
        let state = {tabIndex: tabIndex};

        if (patients && patients.length) {
            let appointment = patients[tabIndex];

            if (!appointment.date_end_appointment)
                state['activeAppointment'] = patients[tabIndex].pk;
            else
                state['activeAppointment'] = -1;
        }
        this.setState(state);
    };

    finishAppointment = () => {
        this.setState({
            activeAppointment: -1
        });
    };

    saveVideo = () => {
        const {activeAppointment} = this.state;
        if (activeAppointment){
            let media = this.state.camera.getRecordedMedia();
            let data = new FormData();
            data.append('video', media.patientVideo);
            data.append('patient_audio', media.patientAudio);
            data.append('doctor_audio', media.doctorAudio);

            this.props.dispatch(saveAppointmentVideo(activeAppointment, data))
        }

    };


    renderPatientsWindow(patients) {
        const renderTabList = () => {
            return (
                <TabList className="nav nav-tabs">
                    <Tab><a className="nav-link">Данные пациента</a></Tab>
                    <Tab><a className="nav-link">Медкарта пациента</a></Tab>
                    <Tab><a className="nav-link">Данные приема</a></Tab>
                    <Tab><a className="nav-link">Чат</a></Tab>
                    <Tab><a className="nav-link">Видеозапись приема</a></Tab>
                </TabList>
            )
        };
        const {isUserReadyForVideoCall} = this.props;
        return (
            <div>
                <Tabs className="fh-breadcrumb" selectedIndex={this.state.tabIndex}
                      onSelect={tabIndex => this.onSelectTab(tabIndex)}>
                    <div className="fh-column">
                        <div className="slimScrollDiv"
                             style={{position: "relative", overflow: "hidden", width: "auto", height: "=100%"}}>
                            <div className="full-height-scroll"
                                 style={{overflow: "hidden", width: "auto", height: "100%"}}>
                                <div className="row">
                                    <div className="col-xs-12 doctor-call-button">
                                        <VideoCallButton buttonName="Начать прием"
                                                         appointment={this.state.activeAppointment}
                                                         finishAppointment={this.finishAppointment}
                                                         disabledButtonName="Завершить прием"/>
                                    </div>
                                    <div className="col-xs-12">
                                        <video autoPlay="autoplay" className="easyrtcMirror selfVideo"
                                               id="selfDoctorVideo" muted="muted" volume="0"/>
                                    </div>
                                </div>
                                <TabList className="list-group elements-list">
                                    {patients && patients.map((patient, index) => {
                                        return (
                                            <Tab className="list-group-item"
                                                 disabled={isUserReadyForVideoCall && patient.pk !== this.state.activeAppointment}
                                                 key={"tab-user-" + patient.pk + 'connected-' + patient.isConnected}>
                                                <a className="nav-link ">
                                                    <span
                                                        className="float-right text-muted"> {patient.time_appointment.time}</span>
                                                    <strong>
                                                        {patient.isConnected ?
                                                            <b>{patient.patient.user_full_name}</b> : patient.patient.user_full_name}
                                                    </strong>
                                                    <div className="small m-t-xs">
                                                        <p>
                                                            Телеприем
                                                        </p>
                                                        <p className="m-b-none">
                                                            <b>{patient.date_end_appointment ? 'Прием завершен' : ''}</b>
                                                        </p>
                                                    </div>
                                                    {patient.isConnected ?
                                                        <VideoCallButtons
                                                            easyrtcid={patient.easyrtcId}
                                                            saveVideo={this.saveVideo}
                                                            camera = {this.state.camera}
                                                        /> : ''}
                                                </a>
                                            </Tab>
                                        )
                                    })}

                                </TabList>

                            </div>
                            <div className="slimScrollBar"
                                 style={{
                                     background: "rgb(0, 0, 0)",
                                     width: "7",
                                     position: "absolute",
                                     top: "0",
                                     opacity: "0.4",
                                     display: "none",
                                     borderRadius: "7",
                                     zIndex: "99",
                                     right: "1",
                                     height: "252.177"
                                 }}>
                            </div>
                            <div className="slimScrollRail"
                                 style={{
                                     width: "7",
                                     height: "100%",
                                     position: "absolute",
                                     top: "0",
                                     display: "none",
                                     borderRadius: "7",
                                     background: "rgb(51, 51, 51)",
                                     opacity: "0.2",
                                     zIndex: "90",
                                     right: "1px"
                                 }}>
                            </div>
                        </div>
                    </div>

                    <div className="full-height">
                        <div className="slimScrollDiv"
                             style={{position: "relative", overflow: "hidden", width: "auto", height: "100%"}}>
                            <div className="full-height-scroll white-bg border-left"
                                 style={{overflow: "hidden", width: "auto", height: "100%"}}>

                                <div className="element-detail-box">
                                    {patients && patients.map(patient => {
                                        return (
                                            <TabPanel className="tab-content"
                                                      key={"tab-panel-user-" + patient.pk + 'connected' + patient.isConnected}>
                                                <div id={"tab-" + patient.pk} className="tab-pane show">
                                                    <Tabs className="tabs-container">
                                                        {renderTabList()}

                                                        <TabPanel>
                                                            <DoctorPatientData/>
                                                        </TabPanel>

                                                        <TabPanel>
                                                            <DoctorMedcartData/>
                                                        </TabPanel>

                                                        <TabPanel>
                                                            <DoctorAppointmentData/>
                                                        </TabPanel>

                                                        <TabPanel>
                                                            <DoctorPatientChat/>
                                                        </TabPanel>
                                                        <TabPanel>
                                                            <VideoViewer video={patient.video_url} audio={patient.total_audio_url}/>
                                                        </TabPanel>

                                                    </Tabs>

                                                </div>

                                            </TabPanel>
                                        )
                                    })}

                                </div>

                            </div>
                            <div className="slimScrollBar"
                                 style={{
                                     background: "rgb(0, 0, 0)",
                                     width: "7",
                                     position: "absolute",
                                     top: "0",
                                     opacity: "0.4",
                                     display: "none",
                                     borderRadius: "7",
                                     zIndex: "99",
                                     right: "1",
                                     height: "264.054"
                                 }}>
                            </div>
                            <div className="slimScrollRail"
                                 style={{
                                     width: "7",
                                     height: "100%",
                                     position: "absolute",
                                     top: "0",
                                     display: "none",
                                     borderRadius: "7",
                                     background: "rgb(51, 51, 51)",
                                     opacity: "0.2",
                                     zIndex: "90",
                                     right: "1px"
                                 }}>
                            </div>
                        </div>
                    </div>

                    <DoctorVideoWindow/>

                </Tabs>
            </div>
        )

    }

    render() {
        const {patients} = this.props;
        if (patients && patients.length)
            return this.renderPatientsWindow(patients);
        else {
            return (
                <div className="ibox">
                    <div className="ibox-title">
                        <h5>На сегодняшний день нет приемов</h5>
                    </div>
                </div>
            )
        }

    }
}

const mapStateToProps = (state) => ({
    patients: serverPatientList(state),
    finishedAppointment: lastFinishedAppointment(state),
    user: serverUserInfo(state),
    isUserReadyForVideoCall: isUserReadyForVideoCall(state),
})

export default connect(mapStateToProps, null)(MedcartList);