import React, {Component} from 'react'
import './VideoWindowIcons.css'

class VideoWindowIcons extends Component {

    constructor(props) {
        super(props);
        this.state = {
            muteAudio: false,
            muteVideo: false,
            isFull: false
        };
    }

    handleClickMuteAudio = (event) => {
        const currentState = this.state.muteAudio;
        this.setState({muteAudio: !currentState});
        window.easyrtc.enableMicrophone(this.state.muteAudio);
    };
    handleClickMuteVideo = (event) => {
        const currentState = this.state.muteVideo;
        this.setState({muteVideo: !currentState});
        window.easyrtc.enableCamera(this.state.muteVideo);
    };

    handleClickFullScreen = (event) => {
        const {setFullWindow} = this.props;
        const currentState = this.state.isFull;
        setFullWindow(!currentState);
        this.setState({isFull: !currentState});
    };

    render() {
        return (
            <div>
                {this.props.callState &&
                    <div id="icons" className="">
                        <div onClick={event => this.handleClickMuteAudio(event)}>
                            <svg id="mute-audio" xmlns="http://www.w3.org/2000/svg" width="48"
                                 height="48" viewBox="-10 -10 68 68" className={this.state.muteAudio ? 'on' : null}>
                                <title>title</title>
                                <circle cx="24" cy="24" r="34">
                                    <title>Выключить звук</title>
                                </circle>
                                <path className="on" transform="scale(0.6), translate(17,18)"
                                      d="M38 22h-3.4c0 1.49-.31 2.87-.87 4.1l2.46 2.46C37.33 26.61 38 24.38 38 22zm-8.03.33c0-.11.03-.22.03-.33V10c0-3.32-2.69-6-6-6s-6 2.68-6 6v.37l11.97 11.96zM8.55 6L6 8.55l12.02 12.02v1.44c0 3.31 2.67 6 5.98 6 .45 0 .88-.06 1.3-.15l3.32 3.32c-1.43.66-3 1.03-4.62 1.03-5.52 0-10.6-4.2-10.6-10.2H10c0 6.83 5.44 12.47 12 13.44V42h4v-6.56c1.81-.27 3.53-.9 5.08-1.81L39.45 42 42 39.46 8.55 6z"
                                      fill="white"/>
                                <path className="off" transform="scale(0.6), translate(17,18)"
                                      d="M24 28c3.31 0 5.98-2.69 5.98-6L30 10c0-3.32-2.68-6-6-6-3.31 0-6 2.68-6 6v12c0 3.31 2.69 6 6 6zm10.6-6c0 6-5.07 10.2-10.6 10.2-5.52 0-10.6-4.2-10.6-10.2H10c0 6.83 5.44 12.47 12 13.44V42h4v-6.56c6.56-.97 12-6.61 12-13.44h-3.4z"
                                      fill="white"/>
                            </svg>
                        </div>
                        <div onClick={event => this.handleClickMuteVideo(event)}>
                            <svg id="mute-video" xmlns="http://www.w3.org/2000/svg" width="48"
                                 height="48" viewBox="-10 -10 68 68" className={this.state.muteVideo ? 'on' : null}>>
                                <circle cx="24" cy="24" r="34">
                                    <title>Выключить видео</title>
                                </circle>
                                <path className="on" transform="scale(0.6), translate(17,16)"
                                      d="M40 8H15.64l8 8H28v4.36l1.13 1.13L36 16v12.36l7.97 7.97L44 36V12c0-2.21-1.79-4-4-4zM4.55 2L2 4.55l4.01 4.01C4.81 9.24 4 10.52 4 12v24c0 2.21 1.79 4 4 4h29.45l4 4L44 41.46 4.55 2zM12 16h1.45L28 30.55V32H12V16z"
                                      fill="white"/>
                                <path className="off" transform="scale(0.6), translate(17,16)"
                                      d="M40 8H8c-2.21 0-4 1.79-4 4v24c0 2.21 1.79 4 4 4h32c2.21 0 4-1.79 4-4V12c0-2.21-1.79-4-4-4zm-4 24l-8-6.4V32H12V16h16v6.4l8-6.4v16z"
                                      fill="white"/>
                            </svg>
                        </div>
                        <div onClick={event => this.handleClickFullScreen(event)}>
                            <svg id="fullscreen" xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="-10 -10 68 68">
                                <circle cx="24" cy="24" r="34">
                                    <title>Полноэкранный режим</title>
                                </circle>
                                <path className="on" transform="scale(0.8), translate(7,6)" d="M10 32h6v6h4V28H10v4zm6-16h-6v4h10V10h-4v6zm12 22h4v-6h6v-4H28v10zm4-22v-6h-4v10h10v-4h-6z" fill="white"/>
                                <path className="off" transform="scale(0.8), translate(7,6)" d="M14 28h-4v10h10v-4h-6v-6zm-4-8h4v-6h6v-4H10v10zm24 14h-6v4h10V28h-4v6zm-6-24v4h6v6h4V10H28z" fill="white"/>
                            </svg>
                        </div>
                    </div>
                }
            </div>
        )
    }
}


export default VideoWindowIcons
