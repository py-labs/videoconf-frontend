import React, {Component} from 'react'

// import './.css';

class DoctorAppointmentData extends Component {
    constructor () {
        super()
        this.state = {
            //startDate: moment()
            startDate: '01.01.2018'
        };
    }

    render() {

        return(
            <div className="row">
                <div className="col-lg-12">
                    <form method="get">
                        <div className="panel-body">
                            <fieldset>
                                <div className="form-group row">
                                    <label className="col-lg-2 col-form-label">Шаблон приема:</label>
                                    <div className="col-lg-10">
                                        <select className="form-control">
                                            <option>Шаблон 1</option>
                                            <option>Шаблон 2</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="hr-line-dashed"/>
                                {/*<div className="form-group row">
                                    <label className="col-sm-1 col-form-label ">Жалобы пациента:</label>
                                    <div className="col-sm-11">
                                        <textarea className="form-control"  style={{height: "95px"}}>Текст</textarea>
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <label className="col-sm-1 col-form-label ">Анамнез заболевания, жизни:</label>
                                    <div className="col-sm-11">
                                        <textarea className="form-control" style={{height: "95px"}}>Текст</textarea>
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <label className="col-sm-1 col-form-label ">Объективные данные</label>
                                    <div className="col-sm-11">
                                        Здесь подгружается шаблон
                                    </div>
                                </div>*/}
                                <div className="form-group row">
                                    <label className="col-sm-12 col-form-label">Заголовок шаблона:</label>
                                </div>
                                <div className="form-group row">
                                    <div className="col-sm-12">
                                        <div className="table-responsive">
                                            <table className="table table-stripped">
                                                <thead>
                                                <tr>
                                                    <th style={{width: '15%'}}>
                                                        Поле шаблона
                                                    </th>
                                                    <th >
                                                        Значение
                                                    </th>
                                                    <th style={{width: '7%'}}>

                                                    </th>
                                                </tr>
                                                </thead>

                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            Жалобы пациента
                                                        </td>
                                                        <td>
                                                            <input type="text" className="form-control" placeholder="" />
                                                        </td>
                                                        <td>
                                                            <button className="btn btn-white"><i className="fa fa-trash" />
                                                            </button>
                                                            <button className="btn btn-white"><i className="fa fa-pencil" />
                                                            </button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Анамнез
                                                        </td>
                                                        <td>
                                                            <input type="text" className="form-control" placeholder="" />
                                                        </td>
                                                        <td>
                                                            <button className="btn btn-white"><i className="fa fa-trash" />
                                                            </button>
                                                            <button className="btn btn-white"><i className="fa fa-pencil" />
                                                            </button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Объективные данные
                                                        </td>
                                                        <td>
                                                            <input type="text" className="form-control" placeholder="" />
                                                        </td>
                                                        <td>
                                                            <button className="btn btn-white"><i className="fa fa-trash" />
                                                            </button>
                                                            <button className="btn btn-white"><i className="fa fa-pencil" />
                                                            </button>
                                                        </td>
                                                    </tr>
                                                </tbody>

                                            </table>
                                            <button className="btn btn-primary" type="submit">Добавить</button>
                                        </div>
                                    </div>
                                </div>
                                <div className="hr-line-dashed"></div>
                                <div className="form-group row">
                                    <label className="col-sm-2 col-form-label ">Диагноз основного заболевания:</label>
                                    <div className="col-sm-7">
                                        <input type="text" className="form-control" placeholder="" />
                                    </div>
                                    <label className="col-sm-1 col-form-label ">код по МКБ-10:</label>
                                    <div className="col-sm-2">
                                        <div className="input-group select">
                                            <div className="input-group-addon">
                                                <i className="fa fa-ellipsis-h"/>
                                            </div>
                                            <input type="text" className="form-control" placeholder="" />
                                        </div>
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <label className="col-sm-2 col-form-label ">Доп. сведения (осложнения):</label>
                                    <div className="col-sm-10">
                                        <input type="text" className="form-control" placeholder="" />
                                    </div>
                                </div>
                                <div className="hr-line-dashed"></div>
                                <div className="form-group row">
                                    <label className="col-sm-12 col-form-label">Сопутствующие заболевания:</label>
                                </div>
                                <div className="form-group row">
                                    <div className="col-sm-12">
                                        <div className="table-responsive">
                                            <table className="table table-stripped">
                                                <thead>
                                                <tr>
                                                    <th>
                                                        Заболевание
                                                    </th>
                                                    <th style={{width: '15%'}}>
                                                        Код по МКБ-10
                                                    </th>
                                                    <th>
                                                        Доп. сведения (осложнения)
                                                    </th>
                                                    <th style={{width: '7%'}}>

                                                    </th>
                                                </tr>
                                                </thead>

                                                <tbody>
                                                <tr>
                                                    <td>
                                                        <input type="text" className="form-control" placeholder="Пример диагноза" />
                                                    </td>
                                                    <td>
                                                        <div className="input-group select">
                                                            <div className="input-group-addon">
                                                                <i className="fa fa-ellipsis-h"/>
                                                            </div>
                                                            <input type="text" className="form-control" placeholder="" />
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <input type="text" className="form-control" placeholder="Пример осложнения" />
                                                    </td>
                                                    <td>
                                                        <button className="btn btn-white"><i className="fa fa-trash" />
                                                        </button>
                                                        <button className="btn btn-white"><i className="fa fa-pencil" />
                                                        </button>
                                                    </td>
                                                </tr>
                                                </tbody>

                                            </table>
                                            <button className="btn btn-primary" type="submit">Добавить</button>
                                        </div>
                                    </div>
                                </div>
                                <div className="hr-line-dashed"></div>
                                <div className="form-group row">
                                    <label className="col-sm-2 col-form-label ">Внешняя причина при травмах (отравлениях):</label>
                                    <div className="col-sm-7">
                                        <input type="text" className="form-control" placeholder="" />
                                    </div>
                                    <label className="col-sm-1 col-form-label ">код по МКБ-10:</label>
                                    <div className="col-sm-2">
                                        <div className="input-group select">
                                            <div className="input-group-addon">
                                                <i className="fa fa-ellipsis-h"/>
                                            </div>
                                            <input type="text" className="form-control" placeholder="" />
                                        </div>
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <label className="col-sm-2 col-form-label ">Группа здоровья:</label>
                                    <div className="col-sm-10">
                                        <select className="form-control">
                                            <option>Группа 1</option>
                                            <option>Группа 2</option>
                                        </select>
                                    </div>
                                </div>

                                <div className="hr-line-dashed"></div>
                                <div className="form-group row">
                                    <label className="col-sm-12 col-form-label">План лечения:</label>
                                </div>
                                <div className="form-group row">
                                    <label className="col-lg-2 col-form-label ">Шаблон лечения:</label>
                                    <div className="col-lg-10">
                                        <select className="form-control">
                                            <option>Шаблон 1</option>
                                            <option>Шаблон 2</option>
                                        </select>
                                    </div>
                                </div>
                                {/*<div className="form-group row">
                                    <label className="col-sm-2 col-form-label ">Назначения (исследования, консультации):</label>
                                    <div className="col-sm-10">
                                        <textarea className="form-control"  style={{height: "95px"}}>Подгружается информация из шаблона</textarea>
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <label className="col-sm-2 col-form-label ">Лекарственные препараты, физиотерапия:</label>
                                    <div className="col-sm-10">
                                        <textarea className="form-control"  style={{height: "95px"}}>Текст</textarea>
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <label className="col-sm-2 col-form-label ">Выданные рецепты:</label>
                                    <div className="col-sm-10">
                                        <textarea className="form-control"  style={{height: "95px"}}>Текст</textarea>
                                    </div>
                                </div>*/}
                                <div className="table-responsive">
                                    <table className="table table-stripped">
                                        <thead>
                                        <tr>
                                            <th style={{width: '15%'}}>
                                                Поле шаблона
                                            </th>
                                            <th >
                                                Значение
                                            </th>
                                            <th style={{width: '7%'}}>

                                            </th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        <tr>
                                            <td>
                                                Назначения
                                            </td>
                                            <td>
                                                <input type="text" className="form-control" placeholder="" />
                                            </td>
                                            <td>
                                                <button className="btn btn-white"><i className="fa fa-trash" />
                                                </button>
                                                <button className="btn btn-white"><i className="fa fa-pencil" />
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Лекарственные препараты, физиотерапия:
                                            </td>
                                            <td>
                                                <input type="text" className="form-control" placeholder="" />
                                            </td>
                                            <td>
                                                <button className="btn btn-white"><i className="fa fa-trash" />
                                                </button>
                                                <button className="btn btn-white"><i className="fa fa-pencil" />
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Выданные рецепты:
                                            </td>
                                            <td>
                                                <input type="text" className="form-control" placeholder="" />
                                            </td>
                                            <td>
                                                <button className="btn btn-white"><i className="fa fa-trash" />
                                                </button>
                                                <button className="btn btn-white"><i className="fa fa-pencil" />
                                                </button>
                                            </td>
                                        </tr>
                                        </tbody>

                                    </table>
                                    <button className="btn btn-primary" type="submit">Добавить</button>
                                </div>
                                <div className="hr-line-dashed"></div>
                                <div className="form-group row">
                                    <label className="col-sm-12 col-form-label">Листок нетрудоспособности, справка:</label>
                                </div>
                                <div className="form-group row">
                                    <div className="col-sm-1">
                                        <select className="form-control">
                                            <option>Да</option>
                                            <option>Нет</option>
                                        </select>
                                    </div>
                                    <label className="col-sm-1 col-form-label ">№:</label>
                                    <div className="col-sm-1">
                                        <div className="input-group date">
                                            <input type="text" className="form-control" value="" />
                                        </div>
                                    </div>
                                    <label className="col-sm-1 col-form-label ">Дата выдачи:</label>
                                    <div className="col-sm-2">
                                        <div className="input-group date">
                                            <span className="input-group-addon"><i className="fa fa-calendar"/></span>
                                            <input type="text" className="form-control" value="" />
                                        </div>
                                    </div>
                                    <label className="col-sm-1 col-form-label ">Дата закрытия:</label>
                                    <div className="col-sm-2">
                                        <div className="input-group date">
                                            <span className="input-group-addon"><i className="fa fa-calendar"/></span>
                                            <input type="text" className="form-control" value="" />
                                        </div>
                                    </div>
                                    <label className="col-sm-1 col-form-label ">Явка на прием:</label>
                                    <div className="col-sm-2">
                                        <div className="input-group date">
                                            <span className="input-group-addon"><i className="fa fa-calendar"/></span>
                                            <input type="text" className="form-control" value="" />
                                        </div>
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <div className="col-sm-11">
                                            <input type="checkbox" id="checkbox1" />&nbsp;
                                            {/*<ins className="iCheck-helper"*/}
                                                 {/*style={{position: "absolute", top: "0%", left: "0%", display: "block", width: "100%", height: "100%", margin: "0px", padding: "0px", background: "rgb(255, 255, 255)",*/}
                                                     {/*border: "0px", opacity: "0"}}>*/}
                                            {/*</ins>*/}
                                            <label for="checkbox1">Информированное добровольное согласие на медицинское вмешательство, отказ от медицинского вмешательства </label>
                                    </div>
                                </div>

                            </fieldset>
                            <div className="form-group row">
                                <div className="col-sm-10 col-sm-offset-0">
                                    {/*<button className="btn btn-white btn-sm" type="submit">Отмена</button>*/}
                                    <button className="btn btn-primary" type="submit">Сохранить</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}

export default DoctorAppointmentData;