import React, {Component} from 'react';
import {Link} from "react-router-dom";
import {connect} from 'react-redux'
import {CHOOSE_APPOINTMENT, requestAppointmentList} from '../../store/appointments/actions'

import {appointmentsList} from '../../store/reducers'
import './PatientAppointmetnsWindow.css'


class PatientAppointmetnsWindow extends Component {
    componentDidMount() {
        this.props.dispatch(requestAppointmentList())
    }

    setSelectedAppointment(appointment) {
        this.props.dispatch({
            type: CHOOSE_APPOINTMENT,
            payload: appointment
        })
    }

    render() {
        const {appointments} = this.props;

        return (
            <div className="ibox">
                <div className="ibox-title justify-content-md-center"><h5>Записи на прием</h5></div>
                <div className="ibox-content">
                    <div className="project-list">
                        <ul className="nav nav-tabs">
                            <li><a className="nav-link active show" data-toggle="tab" href="#tab-1">
                                <i className="fas fa-check-double"/> Предстоящие</a></li>
                            <li><a className="nav-link" data-toggle="tab" href="#tab-2">
                                <i className="fas user-md"/> Архив</a></li>
                        </ul>
                        <table className="table table-hover" id="#tab-1">
                            <tbody>
                            {appointments.map(appointment => {
                                return (
                                    <tr key={appointment.pk}>
                                        <td className="project-status"><span
                                            className="label label-primary">Прием</span></td>
                                        <td className="project-title">
                                            Дата/Время: <strong>{appointment.date_appointment} {appointment.time_appointment.time}</strong>
                                        </td>
                                        <td className="project-completion">
                                            Врач: <strong>{appointment.doctor.user_full_name}</strong>
                                        </td>
                                        <td className="project-people">
                                            <small>Дополнительная информация</small>
                                        </td>

                                        <td className="project-actions">
                                            <Link to="/conf" className="btn btn-white btn-sm"
                                                  onClick={this.setSelectedAppointment.bind(this, appointment)}>
                                                <i className="fa fa-folder"/> Просмотр
                                            </Link>
                                        </td>
                                    </tr>
                                )
                            })}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    appointments: appointmentsList(state),
})


export default connect(mapStateToProps, null)(PatientAppointmetnsWindow);
