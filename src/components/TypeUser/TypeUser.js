import React, {Component} from 'react';
import {connect} from 'react-redux'
import {userInfo} from '../../store/user/actions'

import {getTypeUserText} from '../../store/reducers'
import './TypeUser.css'


class TypeUser extends Component {
    componentDidMount() {
        this.props.dispatch(userInfo())
    }

    render() {
        const {typeUser} = this.props;

        return (
            <span className="text-muted text-xs block">
                    { typeUser }
                <b className="caret"/>
            </span>
        )
    }
}

const mapStateToProps = (state) => ({
    typeUser: getTypeUserText(state),
})


export default connect(mapStateToProps, null)(TypeUser);
