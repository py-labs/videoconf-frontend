import React, {Component} from 'react'
import {Link} from "react-router-dom";
//import './index.scss';

class EventsList extends Component {
    render() {
        return (
            <div className="ibox">
                <div className="ibox-title justify-content-md-center"><h5>Ближайшие события</h5></div>
                <div className="ibox-content">
                    <div className="project-list">
                        <table className="table table-hover">
                            <tbody>
                            <tr>
                                <td className="project-status"><span className="label label-primary">Прием</span></td>
                                <td className="project-title">
                                    <strong>14.08.2018 16:30</strong>
                                </td>
                                <td className="project-completion">
                                    Прием у гастроэнтеролога. Не пить не курить за 3 часа, с собой пеленку.
                                </td>
                                <td className="project-actions">
                                    <Link to="/events" className="btn btn-white btn-sm">
                                        <i className="fa fa-trash" /> Отменить
                                    </Link>
                                </td>
                            </tr>
                            <tr>
                                <td className="project-status"><span className="label label-warning">Обсл</span></td>
                                <td className="project-title">
                                    <strong>17.08.2018 7:30</strong>
                                </td>
                                <td className="project-completion">
                                    Сдача крови на биохимический анализ. Строга на тощак, не пить, не курить.
                                </td>
                                <td className="project-actions">
                                    <Link to="/events" className="btn btn-white btn-sm">
                                        <i className="fa fa-trash" /> Отменить
                                    </Link>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        )
    }
}

export default EventsList;