import React, {Component} from 'react'
import "./MedcartDetail.css"
import {Link} from "react-router-dom";

class MedcartDetail extends Component {
    render() {
        return(
            <div className="row">
                <div className="col-lg-12">
                    <div className="wrapper wrapper-content {/*animated fadeInUp*/}">
                        <div className="ibox">
                            <div className="ibox-content">
                                <div className="row">
                                    <div className="col-lg-12">
                                        <div className="m-b-md">
                                            <Link to="/medcart" className="btn btn-primary btn-xs float-right">
                                                К списку
                                            </Link>
                                            <h2>Прием специалиста № БМ-0002610 от 30.11.2018 16:40</h2>
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-lg-6">
                                        <dl className="row mb-0">
                                            <div className="col-sm-4 text-sm-right">
                                                <dt>Врач:</dt>
                                            </div>
                                            <div className="col-sm-8 text-sm-left">
                                                <dd className="mb-1">Логинов Александр Николаевич</dd>
                                            </div>
                                        </dl>
                                        <dl className="row mb-0">
                                            <div className="col-sm-4 text-sm-right">
                                                <dt>Специализация:</dt>
                                            </div>
                                            <div className="col-sm-8 text-sm-left">
                                                <dd className="mb-1">Гинеколог</dd>
                                            </div>
                                        </dl>

                                    </div>
                                    <div className="col-lg-6" id="cluster_info">

                                        <dl className="row mb-0">
                                            <div className="col-sm-4 text-sm-right">
                                                <dt>Случай:</dt>
                                            </div>
                                            <div className="col-sm-8 text-sm-left">
                                                <dd className="mb-1">Амбулаторная помощь</dd>
                                            </div>
                                        </dl>
                                        <dl className="row mb-0">
                                            <div className="col-sm-4 text-sm-right">
                                                <dt>Начало случая:</dt>
                                            </div>
                                            <div className="col-sm-8 text-sm-left">
                                                <dd className="mb-1">30.11.2018</dd>
                                            </div>
                                        </dl>
                                    </div>
                                </div>
                                <div className="row m-t-sm">
                                    <div className="col-lg-12">
                                        <div className="panel blank-panel">
                                            <div className="panel-heading">
                                            </div>

                                            <div className="panel-body">
                                                <div className="feed-activity-list">
                                                    <div className="feed-element">
                                                        <div className="media-body ">
                                                            <strong>Диагноз основного заболевания</strong>
                                                            <br/>
                                                            <div className="well">
                                                                Острый вагинит; N76.0
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div className="feed-element">
                                                        <div className="media-body ">
                                                            <strong>Сопутствующие заболевания</strong>
                                                            <br/>
                                                            <div className="well">
                                                                Кандидоз влагалища. Уреаплазмоз.
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="feed-element">
                                                        <div className="media-body ">
                                                            <small className="float-right">
                                                            </small>
                                                            <strong>Объективные данные</strong>
                                                            <br/>
                                                            <div className="well">

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="feed-element">
                                                        <div className="media-body ">
                                                            <small className="float-right">
                                                            </small>
                                                            <strong>Лечебные назначения</strong>
                                                            <br/>
                                                            <div className="well">
                                                                нистатин 1 таб ( 500 000 ед) х 4 раза в сутки х 7 дней <br />
                                                                затем Ацилакт 1 св на ночь вагинально х 10 дней <br />
                                                                Генферон 1 св ( 1000 000 ед) ректально на ночь х 10 дней <br />
                                                                контроль посева уреаплазмы на чувствительность через 3-6 мес <br />
                                                                фурадонин 1 таб Х 4 раза в сутки х 5 дней
                                                                {/*<table className="table table-striped">
                                                                    <thead>
                                                                    <tr>
                                                                        <th>Препарат</th>
                                                                        <th>Дозировка</th>
                                                                        <th>Схема приема</th>
                                                                        <th>Комментарии</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    <tr>
                                                                        <td>
                                                                            Название препарата
                                                                        </td>
                                                                        <td>
                                                                            0,25 мг
                                                                        </td>
                                                                        <td>
                                                                            по 1 табл. 3 раза в день после еды
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Название препарата
                                                                        </td>
                                                                        <td>
                                                                            0,5 мг
                                                                        </td>
                                                                        <td>
                                                                            по 1 табл. 1 раза в день после еды
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                    </tr>

                                                                    </tbody>
                                                                </table>*/}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="feed-element">
                                                        <div className="media-body ">
                                                            <small className="float-right">
                                                            </small>
                                                            <strong>Файлы</strong>
                                                            <br/>
                                                            <div className="well">
                                                                <ul className="list-unstyled project-files">
                                                                    <li>
                                                                        <a href=""><i
                                                                            className="fa fa-file"/> Документ.docx
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href=""><i
                                                                            className="fa fa-file-picture-o"/> Снимок.jpg
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {/*<div className="feed-element">
                                                        <div className="media-body ">
                                                            <small className="float-right">
                                                            </small>
                                                            <strong>Комментарии</strong>
                                                            <br/>
                                                            <div className="well">
                                                                <table className="table table-striped">
                                                                    <tbody>
                                                                    <tr>
                                                                        <td>
                                                                            Текст комментария пользователя
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <div className="col-md-2">
                                                            <button type="button"
                                                                    className="btn btn-primary btn-sm btn-block">
                                                                <i className="fa fa-comment"></i> Добавить
                                                                комментарий
                                                            </button>
                                                        </div>

                                                    </div>*/}

                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default MedcartDetail;