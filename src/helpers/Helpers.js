import $ from 'jquery';

export function correctHeight() {

    var pageWrapper = $('#page-wrapper');
    var navbarHeight = $('nav.navbar-default').height();
    var wrapperHeigh = pageWrapper.height();

    if (navbarHeight > wrapperHeigh) {
        pageWrapper.css("min-height", navbarHeight + "px");
    }

    if (navbarHeight <= wrapperHeigh) {
        if (navbarHeight < $(window).height()) {
            pageWrapper.css("min-height", $(window).height() + "px");
        } else {
            pageWrapper.css("min-height", navbarHeight + "px");
        }
    }

    if ($('body').hasClass('fixed-nav')) {
        if (navbarHeight > wrapperHeigh) {
            pageWrapper.css("min-height", navbarHeight + "px");
        } else {
            pageWrapper.css("min-height", $(window).height() - 60 + "px");
        }
    }
}

export function detectBody() {
    if ($(document).width() < 769) {
        $('body').addClass('body-small')
    } else {
        $('body').removeClass('body-small')
    }
}

export function smoothlyMenu() {
    if (!$('body').hasClass('mini-navbar') || $('body').hasClass('body-small')) {
        // Hide menu in order to smoothly turn on when maximize menu
        $('#side-menu').hide();
        // For smoothly turn on menu
        setTimeout(
            function () {
                $('#side-menu').fadeIn(400);
            }, 200);
    } else if ($('body').hasClass('fixed-sidebar')) {
        $('#side-menu').hide();
        setTimeout(
            function () {
                $('#side-menu').fadeIn(400);
            }, 100);
    } else {
        // Remove all inline style from jquery fadeIn function to reset menu state
        $('#side-menu').removeAttr('style');
    }
}

export function isEmpty(obj) {
    return Object.keys(obj).length === 0;
}

export class CameraContainer {
    constructor(props) {
    }

    getOnlyAudioStream(stream) {
        let audioOnlyStream = new MediaStream();
        stream.getAudioTracks().forEach(function (track) {
            audioOnlyStream.addTrack(track);
        });
        return audioOnlyStream;
    }

    getOnlyVideoStream(stream) {
        let videoOnlyStream = new MediaStream();
        stream.getVideoTracks().forEach(function (track) {
            videoOnlyStream.addTrack(track);
        });
        return videoOnlyStream;
    }

    init() {
        let doctorAudioStream = this.getOnlyAudioStream(window.easyrtc.getLocalStream());

        let doctorAudioStreamOptions = {mimeType: 'audio/webm'};
        let patientAudioStream = this.getOnlyAudioStream(window.easyrtc.getRemoteStream(window.easyrtc.getIthCaller(0), 'audio'));
        let patientAudioStreamOptions = {mimeType: 'audio/webm'};
        let patientVideoStream = window.easyrtc.getRemoteStream(window.easyrtc.getIthCaller(0), 'video');
        let patientVideoStreamOptions = {mimeType: 'video/webm;codecs=vp9'};
        this.doctorAudio = new Camera({'stream': doctorAudioStream, 'options': doctorAudioStreamOptions});
        this.patientAudio = new Camera({'stream': patientAudioStream, 'options': patientAudioStreamOptions});
        this.patientVideo = new Camera({'stream': patientVideoStream, 'options': patientVideoStreamOptions});
        this.doctorAudio.init();
        this.patientAudio.init();
        this.patientVideo.init();
    }

    startRecording() {
        this.doctorAudio.mediaRecorder.start(100);
        this.patientAudio.mediaRecorder.start(100);
        this.patientVideo.mediaRecorder.start(100);
    }

    stopRecording() {
        this.doctorAudio.mediaRecorder.stop();
        this.patientAudio.mediaRecorder.stop();
        this.patientVideo.mediaRecorder.stop();
    }

    getRecordedMedia() {
        return {
            'doctorAudio': new Blob(this.doctorAudio.recordedBlobs, {type: 'audio/webm'}),
            'patientAudio': new Blob(this.patientAudio.recordedBlobs, {type: 'audio/webm'}),
            'patientVideo': new Blob(this.patientVideo.recordedBlobs, {type: 'video/webm'})
        };
    }
}

export class Camera {
    constructor(props) {
        this.stream = props.stream;
        this.options = props.options;
        this.recordedBlobs = [];
        this.mediaRecorder = null;
    }

    init() {
        this.mediaRecorder = new MediaRecorder(this.stream, this.options);
        this.mediaRecorder.ondataavailable = (event) => {
            if (event.data && event.data.size > 0) {
                this.recordedBlobs.push(event.data)
            }
        };
    }

    startRecording() {
        this.mediaRecorder.start(100);
    }

    stopRecording() {
        this.mediaRecorder.stop();
    }

    getRecordedVideoBlob() {
        return new Blob(this.recordedBlobs, {type: 'video/webm'});
    }
}