import {combineReducers} from 'redux';
import {routerReducer} from 'react-router-redux';
import auth, * as fromAuth from './auth/reducer';
import doctor, * as fromDoctor from './doctor/reducer'
import user, * as fromUser from './user/reducer'
import patient, * as fromPatient from './patient/reducer'
import appointments, * as fromAppointments from './appointments/reducer'

export default combineReducers({
    auth: auth,
    doctor: doctor,
    user: user,
    appointments: appointments,
    patient: patient,
    router: routerReducer
})

export const userLogin = state => fromAuth.userLogin(state.auth);
export const isAuthenticated = state => fromAuth.isAuthenticated(state.auth);
export const accessToken = state => fromAuth.accessToken(state.auth);
export const isAccessTokenExpired = state => fromAuth.isAccessTokenExpired(state.auth);
export const refreshToken = state => fromAuth.refreshToken(state.auth);
export const isRefreshTokenExpired = state => fromAuth.isRefreshTokenExpired(state.auth);
export const authErrors = state => fromAuth.errors(state.auth);

//doctor state
export const serverPatientList = state => fromDoctor.serverPatientList(state.doctor);
export const lastFinishedAppointment = state => fromDoctor.lastFinishedAppointment(state.doctor);

//user state
export const serverUserInfo = state => fromUser.serverUserInfo(state.user);
export const getTypeUserText = state => fromUser.getTypeUserText(state.user);
export const isUserDoctor = state => fromUser.isUserDoctor(state.user);
export const getUserFullName = state => fromUser.getUserFullName(state.user);
export const isUserReadyForVideoCall = state => fromUser.isUserReadyForVideoCall(state.user);
export const getPatientProfile = state => fromUser.getPatientProfile(state.user);

export const appointmentsList = state => fromAppointments.appointmentList(state.appointments);
export const selectedAppointment = state => fromAppointments.selectedAppointment(state.appointments);
export const selectedAppointmentDoctorFullName = state => fromAppointments.selectedAppointmentDoctorFullName(state.appointments);
export const selectedAppointmentDate = state => fromAppointments.selectedAppointmentDate(state.appointments);
export const recordList = state => fromAppointments.recordList(state.appointments);
export const patientTimeRecord = state => fromAppointments.selectedPatientTimeRecord(state.appointments);

//patient state
export const serverSpecializationList = state => fromPatient.serverSpecializationList(state.patient);
export const serverDoctorSpecializationList = state => fromPatient.serverDoctorSpecializationList(state.patient);
export const serverDoctorPlaceList = state => fromPatient.serverDoctorPlaceList(state.patient);
export const calendarSelectedFilters = state => fromPatient.calendarSelectedFilters(state.patient);




export function withAuth(headers={}) {
    return (state) => ({
        ...headers,
        'Authorization': `Bearer ${accessToken(state)}`
    })
}