import * as patient from './actions'

const initialState = {
    specializations: [],
    doctors: [],
    places: [],
    calendarSelectedFilters: {}
}

export default (state = initialState, action) => {
    switch (action.type) {
        case patient.SPECIALIZATION_LIST_SUCCESS:
            return {
                ...state,
                specializations: action.payload
            };
        case patient.DOCTOR_SPECIALIZATION_LIST_SUCCESS:
            return {
                ...state,
                doctors: action.payload
            };
        case patient.DOCTOR_PLACE_LIST_SUCCESS:
            return {
                ...state,
                places: action.payload
            };
        case patient.SET_SELECTED_CALENDAR_FILTERS:
            return {
                ...state,
                calendarSelectedFilters:action.payload
            };
        default:
            return state
    }
}


export const serverSpecializationList = (state) => state.specializations;
export const serverDoctorSpecializationList = (state) => state.doctors;
export const serverDoctorPlaceList = (state) => state.places;
export const calendarSelectedFilters = (state) => state.calendarSelectedFilters;
