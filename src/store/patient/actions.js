import {RSAA} from 'redux-api-middleware';
import {withAuth} from '../reducers'

export const SPECIALIZATION_LIST_REQUEST = '@@patient/SPECIALIZATION_LIST_REQUEST';
export const SPECIALIZATION_LIST_SUCCESS = '@@patient/SPECIALIZATION_LIST_SUCCESS';
export const SPECIALIZATION_LIST_FAILURE = '@@patient/SPECIALIZATION_LIST_FAILURE';

export const DOCTOR_SPECIALIZATION_LIST_REQUEST = '@@patient/DOCTOR_SPECIALIZATION_LIST_REQUEST';
export const DOCTOR_SPECIALIZATION_LIST_SUCCESS = '@@patient/DOCTOR_SPECIALIZATION_LIST_SUCCESS';
export const DOCTOR_SPECIALIZATION_LIST_FAILURE = '@@patient/DOCTOR_SPECIALIZATION_LIST_FAILURE';

export const DOCTOR_PLACE_LIST_REQUEST = '@@patient/DOCTOR_PLACE_LIST_REQUEST';
export const DOCTOR_PLACE_LIST_SUCCESS = '@@patient/DOCTOR_PLACE_LIST_SUCCESS';
export const DOCTOR_PLACE_LIST_FAILURE = '@@patient/DOCTOR_PLACE_LIST_FAILURE';

export const SET_SELECTED_CALENDAR_FILTERS = '@@patient/SET_SELECTED_CALENDAR_FILTERS';


export const requestSpecializationList = () => ({
    [RSAA]: {
        endpoint: '/api/v1/specializations/',
        method: 'GET',
        headers: withAuth({'Content-Type': 'application/json'}),
        types: [
            SPECIALIZATION_LIST_REQUEST, SPECIALIZATION_LIST_SUCCESS, SPECIALIZATION_LIST_FAILURE
        ]
    }
});

export const requestDoctorSpecializationList = (specialization) => ({
    [RSAA]: {
        endpoint: `/api/v1/doctors/?specialization=${specialization}`,
        method: 'GET',
        headers: withAuth({'Content-Type': 'application/json'}),
        types: [
            DOCTOR_SPECIALIZATION_LIST_REQUEST, DOCTOR_SPECIALIZATION_LIST_SUCCESS, DOCTOR_SPECIALIZATION_LIST_FAILURE
        ]
    }
});

export const requestDoctorPlaceList = () => ({
    [RSAA]: {
        endpoint: '/api/v1/places/',
        method: 'GET',
        headers: withAuth({'Content-Type': 'application/json'}),
        types: [
            DOCTOR_PLACE_LIST_REQUEST, DOCTOR_PLACE_LIST_SUCCESS, DOCTOR_PLACE_LIST_FAILURE
        ]
    }
});