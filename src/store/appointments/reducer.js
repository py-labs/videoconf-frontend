import * as appointments from './actions'

const initialState = {
    entries: [],
    selectedAppointment: {
        doctor: {
            user_full_name: ''
        },
        date_appointment: ''
    },
    records: [],

    selectedTimeRecord: {}
};

export default (state = initialState, action) => {
    switch (action.type) {
        case appointments.APPOINTMENTS_INFO_SUCCESS:
            return {
                ...state,
                entries: action.payload
            };
        case appointments.CHOOSE_APPOINTMENT:
            return {
                ...state,
                selectedAppointment: action.payload
            };
        case appointments.RECORDS_APPOINTMENTS_SUCCESS:
            return {
                ...state,
                records: action.payload
            };
        case appointments.SET_AVAILABLE_TIME:
            return {
                ...state,
                selectedTimeRecord: action.payload
            };
        case appointments.CREATE_APPOINTMENT_REQUEST:
            return state;
        case appointments.CREATE_APPOINTMENT_SUCCESS:
            let newRecords = [];
            let response = action.payload;

            // делаю новый список с временами
            // пробовал делать на основе старого, удалять в нем, редакс не видит изменений в нем
            for(let i=0; i< state.records.length; i++){
                let record = state.records[i];
                if (record.date_record === response.date_appointment && record.doctor.pk === response.doctor){
                    let times = [];
                    for (let j = 0; j < record.available_time.length; j++){
                        let time = record.available_time[j];
                        if (time.id !== response.time_appointment){
                            times.push(time);
                        }
                    }

                    newRecords.push({
                        'date_record': record.date_record,
                        'available_time': times,
                        'doctor': record.doctor,
                        'place': record.place
                    })

                }
                else {
                    newRecords.push(record);
                }
            }

            // //delete with no times
            // for (let i = times.length - 1; i >= 0; --i) {
            //     if (times[i].times.length === 0){
            //         times.splice(i, 1)
            //     }
            // }
            return {
                ...state,
                records: newRecords
            };
        case appointments.CREATE_APPOINTMENT_FAILURE:
            return state;
        case appointments.CLEAR_APPOINTMENTS:
            return {
                ...state,
                records: [],
                selectedTimeRecord: {}
            };
        default:
            return state
    }
}


export const appointmentList = (state) => state.entries;
export const selectedAppointment = (state) => state.selectedAppointment;
export const selectedAppointmentDoctorFullName = (state) => state.selectedAppointment.doctor.user_full_name;
export const selectedAppointmentDate = (state) => state.selectedAppointment.date_appointment;
export const recordList = (state) => state.records;
export const selectedPatientTimeRecord = (state) => state.selectedTimeRecord;