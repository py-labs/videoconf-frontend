import {RSAA} from 'redux-api-middleware';
import {withAuth} from '../reducers'

export const APPOINTMENTS_INFO_REQUEST = '@@appointments/APPOINTMENTS_INFO_REQUEST';
export const APPOINTMENTS_INFO_SUCCESS = '@@appointments/APPOINTMENTS_INFO_SUCCESS';
export const APPOINTMENTS_INFO_FAILURE = '@@appointments/APPOINTMENTS_INFO_FAILURE';
export const RECORDS_APPOINTMENTS_REQUEST = '@@appointments/RECORDS_APPOINTMENTS_REQUEST';
export const RECORDS_APPOINTMENTS_SUCCESS = '@@appointments/RECORDS_APPOINTMENTS_SUCCESS';
export const RECORDS_APPOINTMENTS_FAILURE = '@@appointments/RECORDS_APPOINTMENTS_FAILURE';
export const CHOOSE_APPOINTMENT = '@@appointments/CHOOSE_APPOINTMENT';
export const SET_AVAILABLE_TIME = '@@appointments/SET_AVAILABLE_TIME';
export const CREATE_APPOINTMENT_REQUEST = '@@appointments/CREATE_APPOINTMENT_REQUEST';
export const CREATE_APPOINTMENT_SUCCESS = '@@appointments/CREATE_APPOINTMENT_SUCCESS';
export const CREATE_APPOINTMENT_FAILURE = '@@appointments/CREATE_APPOINTMENT_FAILURE';
export const CLEAR_APPOINTMENTS = '@@appointments/CLEAR_APPOINTMENTS';


export const requestAppointmentList = () => ({
    [RSAA]: {
        endpoint: '/api/v1/appointments/',
        method: 'GET',
        headers: withAuth({'Content-Type': 'application/json'}),
        types: [
            APPOINTMENTS_INFO_REQUEST, APPOINTMENTS_INFO_SUCCESS, APPOINTMENTS_INFO_FAILURE
        ]
    }
});

export const requestRecordsAppointments = (dateStart, dateEnd, doctor, specialization, place) => ({
    [RSAA]: {
        endpoint: `/api/v1/doctors-appointments/?date_record_after=${dateStart}&date_record_before=${dateEnd}&doctor=${doctor}&doctor__specialization=${specialization}&place=${place}`,
        method: 'GET',
        headers: withAuth({'Content-Type': 'application/json'}),
        types: [
            RECORDS_APPOINTMENTS_REQUEST, RECORDS_APPOINTMENTS_SUCCESS, RECORDS_APPOINTMENTS_FAILURE
        ]
    }
});

export const requestCreateAppointment = (doctor, patient, date, time, place) => ({
    [RSAA]: {
        endpoint: '/api/v1/appointments/',
        method: 'POST',
        body: JSON.stringify({
            doctor: doctor,
            patient: patient,
            date_appointment: date,
            time_appointment: time,
            place: place
        }),
        headers: withAuth({'Content-Type': 'application/json'}),
        types: [
            CREATE_APPOINTMENT_REQUEST, CREATE_APPOINTMENT_SUCCESS, CREATE_APPOINTMENT_FAILURE
        ]
    }
})