import {CALL_API, RSAA} from 'redux-api-middleware';
import {withAuth} from '../reducers'

export const PATIENT_LIST_REQUEST = '@@doctor/PATIENT_LIST_REQUEST';
export const PATIENT_LIST_SUCCESS = '@@doctor/PATIENT_LIST_SUCCESS';
export const PATIENT_LIST_FAILURE = '@@doctor/PATIENT_LIST_FAILURE';
export const UPDATE_PATIENT_LIST = '@@doctor/UPDATE_PATIENT_LIST';
export const START_APPOINTMENT_REQUEST = '@@doctor/START_APPOINTMENT_REQUEST';
export const START_APPOINTMENT_SUCCESS = '@@doctor/START_APPOINTMENT_SUCCESS';
export const START_APPOINTMENT_FAILURE = '@@doctor/START_APPOINTMENT_FAILURE';

export const END_APPOINTMENT_REQUEST = '@@doctor/END_APPOINTMENT_REQUEST';
export const END_APPOINTMENT_FAILURE = '@@doctor/END_APPOINTMENT_FAILURE';
export const END_APPOINTMENT_SUCCESS = '@@doctor/END_APPOINTMENT_SUCCESS';

export const APPOINTMENT_VIDEO_SUCCESS = '@@doctor/APPOINTMENT_VIDEO_SUCCESS';
export const APPOINTMENT_VIDEO_REQUEST = '@@doctor/APPOINTMENT_VIDEO_REQUEST';
export const APPOINTMENT_VIDEO_FAILURE = '@@doctor/APPOINTMENT_VIDEO_FAILURE';


export const appointmentList = () => ({
    [RSAA]: {
        endpoint: '/api/v1/appointments/',
        method: 'GET',
        headers: withAuth({'Content-Type': 'application/json'}),
        types: [
            PATIENT_LIST_REQUEST, PATIENT_LIST_SUCCESS, PATIENT_LIST_FAILURE
        ]
    }
});

export const saveAppointmentStart = (appointment) => ({
    [RSAA]: {
        endpoint: '/api/v1/appointments/'+ appointment + '/',
        method: 'PUT',
        body: JSON.stringify({
            pk: appointment,
            partial: true,
            date_start_appointment: true
        }),
        headers: withAuth({'Content-Type': 'application/json'}),
        types: [
            START_APPOINTMENT_REQUEST, START_APPOINTMENT_SUCCESS, START_APPOINTMENT_FAILURE
        ]
    }
});

export const saveAppointmentEnd = (appointment) => ({
    [RSAA]: {
        endpoint: '/api/v1/appointments/'+ appointment + '/',
        method: 'PUT',
        body: JSON.stringify({
            pk: appointment,
            partial: true,
            date_end_appointment: true
        }),
        headers: withAuth({'Content-Type': 'application/json'}),
        types: [
            END_APPOINTMENT_REQUEST, END_APPOINTMENT_SUCCESS, END_APPOINTMENT_FAILURE
        ]
    }
})

export const saveAppointmentVideo = (appointment, video) => ({
    [CALL_API]: {
        endpoint: '/api/v1/appointments/'+ appointment + '/',
        method: 'PUT',
        body: video,
        types: [
            APPOINTMENT_VIDEO_REQUEST, APPOINTMENT_VIDEO_SUCCESS, APPOINTMENT_VIDEO_FAILURE
        ],
        headers: withAuth(),
    }
})