import * as doctor from './actions'

const initialState = {
    patients: [],
    connectedPatients: {},
    lastFinishedAppointment: null
};

function createConnectedPatientList(oldPatients, connectedPatients) {
    let patients = [];
    for (let i = 0; i < oldPatients.length; i++) {
        let patient = oldPatients[i];
        let patientUsername = patient.patient.username;

        if (connectedPatients.hasOwnProperty(patientUsername)) {
            patient['easyrtcId'] = connectedPatients[patientUsername];
            patient['isConnected'] = true;
        }
        else {
            patient['isConnected'] = false;
        }
        patients.push(patient);
    }
    return patients;
}


export default (state = initialState, action) => {
    switch (action.type) {
        case doctor.PATIENT_LIST_SUCCESS:
            let newPatients = createConnectedPatientList(action.payload, state.connectedPatients);
            return {
                ...state,
                patients: newPatients
            };

        case doctor.UPDATE_PATIENT_LIST:
            let patients = createConnectedPatientList(state.patients, action.payload);

            return {
                patients: patients,
                connectedPatients: action.payload
            };
        case doctor.END_APPOINTMENT_SUCCESS:
            return {
                ...state,
                lastFinishedAppointment: action.payload.pk
            };
        case doctor.APPOINTMENT_VIDEO_REQUEST:
            console.log('request');
            return state;
        case doctor.APPOINTMENT_VIDEO_SUCCESS:
            console.log('success');
            return state;
        case doctor.APPOINTMENT_VIDEO_FAILURE:
            console.log('failure');
            return state;
        default:
            return state
    }
}


export const serverPatientList = (state) => state.patients;
export const lastFinishedAppointment = (state) => state.lastFinishedAppointment;

