import * as user from './actions'

const USER_DOCTOR_TEXT = 'Доктор';
const USER_PATIENT_TEXT = 'Пациент';

const initialState = {
    id:'',
    username: '',
    userFullName: '',
    isDoctor: false,
    typeUserText: USER_PATIENT_TEXT,
    isReadyForVideoCall: false,
    doctor: {},
    patient: {}
};

export default (state = initialState, action) => {
    switch (action.type) {
        case user.USER_INFO_SUCCESS:
            const isDoctor = !!(action.payload.doctor && action.payload.doctor.hasOwnProperty('pk'));
            return {
                ...state,
                id: action.payload.id,
                username: action.payload.username,
                userFullName: action.payload.user_full_name,
                isDoctor: isDoctor,
                typeUserText: isDoctor ? USER_DOCTOR_TEXT: USER_PATIENT_TEXT,
                doctor: action.payload.doctor,
                patient: action.payload.patient
            };
        case user.USER_READY_FOR_VIDEO_CALL:
            return {
                ...state,
                isReadyForVideoCall:true
            };
        case user.USER_NOT_READY_FOR_VIDEO_CALL:
            return {
                ...state,
                isReadyForVideoCall:false
            };
        default:
            return state
    }
}


export const serverUserInfo = (state) => state;
export const getTypeUserText = (state) => state.typeUserText;
export const isUserDoctor = (state) => state.isDoctor;
export const getUserFullName = (state) => state.userFullName;
export const isUserReadyForVideoCall = (state) => state.isReadyForVideoCall;
export const getPatientProfile = (state) => state.patient;