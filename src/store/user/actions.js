import {RSAA} from 'redux-api-middleware';
import {withAuth} from '../reducers'

export const USER_INFO_REQUEST = '@@user/USER_INFO_REQUEST';
export const USER_INFO_SUCCESS = '@@user/USER_INFO_SUCCESS';
export const USER_INFO_FAILURE = '@@user/USER_INFO_FAILURE';
export const USER_READY_FOR_VIDEO_CALL = '@@user/USER_READY_FOR_VIDEO_CALL';
export const USER_NOT_READY_FOR_VIDEO_CALL = '@@user/USER_NOT_READY_FOR_VIDEO_CALL';


export const userInfo = () => ({
    [RSAA]: {
        endpoint: '/api/v1/user-info/',
        method: 'GET',
        headers: withAuth({'Content-Type': 'application/json'}),
        types: [
            USER_INFO_REQUEST, USER_INFO_SUCCESS, USER_INFO_FAILURE
        ]
    }
});